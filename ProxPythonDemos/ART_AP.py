
import SetProxPythonPath
from proxtoolbox.experiments.CT.ART_Experiment import ART_Experiment

ART = ART_Experiment(algorithm='AP', anim=True, anim_step=1)
ART.run()
ART.show()
