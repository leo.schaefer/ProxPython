
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='AvP2', lambda_0=0.75, lambda_max=0.75,
                     MAXIT=6000, TOL=5e-10, warmup_iter=50)
CDP.run()
CDP.show()
