
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment


CDP = CDP_Experiment(algorithm='CDRl', formulation='cyclic', lambda_0=0.33, lambda_max=0.33,
                     MAXIT=1000)
CDP.run()
CDP.show()
