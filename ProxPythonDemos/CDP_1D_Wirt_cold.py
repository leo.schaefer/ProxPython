
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

CDP = CDP_Experiment(algorithm='Wirtinger')
CDP.run()
CDP.show()
