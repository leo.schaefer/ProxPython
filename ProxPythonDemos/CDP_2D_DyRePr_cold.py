
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDP_Experiment import CDP_Experiment

CDP = CDP_Experiment(algorithm='DyRePr', Nx=256, TOL=1e-8, data_ball = 1e-15)
CDP.run()
CDP.show()
