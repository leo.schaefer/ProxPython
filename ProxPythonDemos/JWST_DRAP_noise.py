
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='DRAP', lambda_0=0.25, lambda_max=0.25,
                       noise=True)
JWST.run()
JWST.show()
