# old demo

import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm = 'HPR', lambda_0 = 0.85, lambda_max = 0.85,
                       MAXIT = 5000, anim=True)
JWST.run()
JWST.show()
