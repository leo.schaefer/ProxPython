
import SetProxPythonPath
from proxtoolbox.experiments.phase.JWST_Experiment import JWST_Experiment

JWST = JWST_Experiment(algorithm='PHeBIE', anim=True)
JWST.run()
JWST.show()
