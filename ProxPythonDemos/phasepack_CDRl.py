
import SetProxPythonPath
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment

phasepack = Phasepack_Experiment(algorithm='CDRl', formulation='cyclic', 
                                 MAXIT=200, lambda_0=0.75, lambda_max=0.75)
phasepack.run()
phasepack.show()
