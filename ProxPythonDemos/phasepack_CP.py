
import SetProxPythonPath
from proxtoolbox.experiments.phase.Phasepack_Experiment import Phasepack_Experiment

phasepack = Phasepack_Experiment(algorithm='CP', formulation='cyclic')
phasepack.run()
phasepack.show()
