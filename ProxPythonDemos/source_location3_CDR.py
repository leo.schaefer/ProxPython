
import SetProxPythonPath
from proxtoolbox.experiments.sourceloc.sourceLocExperiment import SourceLocExperiment

sourceExp = SourceLocExperiment(algorithm='CDRl', formulation='cyclic', lambda_0=1, lambda_max=1)
sourceExp.run()
sourceExp.show()
