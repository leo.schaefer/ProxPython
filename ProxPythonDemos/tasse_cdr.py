
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'CDRl', lambda_0 = 1.0, lambda_max = 1.0, MAXIT = 2000)
CDI.run()
CDI.show()
