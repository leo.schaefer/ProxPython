
import SetProxPythonPath
from proxtoolbox.experiments.phase.CDI_Experiment import CDI_Experiment

CDI = CDI_Experiment(algorithm = 'CDRl', lambda_0 = 0.9, lambda_max = 0.9)
CDI.run()
CDI.show()
