
******
Manual
******

The ProxToolbox is divided into the following packages:

.. toctree::
   :maxdepth: 1

   src/proxtoolbox.experiments
   src/proxtoolbox.algorithms
   src/proxtoolbox.proxoperators
   src/proxtoolbox.utils


A full list of packages can be found at:

.. toctree::
   :maxdepth: 1

   src/modules