# Adding variables and functions to an Experiment

*A good resource for learning about Python (and its classes) is our Project called [M.O.P.](https://nam.pages.gwdg.de/mop/src/Objektorientierung/objektorientierung_lecture.html)*

Suppose **new_var** is the variable you want to add to the experiment **exper** and use it in a newly defined function **func**:

## 1. Initialize new data attribute
Add **new_var** to the constructor `__init__` as argument and then set `self.new_var = new_var` in the body:
``` python
class exper:
    def __init__(self, new_var):
        self.new_var = new_var
```

## 2. Define new method
Add the definition of the function **func** below using `self.new_var` to refer to the newly added variable:

``` python
class exper:
    def __init__(self, new_var):
        self.new_var = new_var

    def func(self):
      return (self.new_var)
```

## 3. Run a test
Test your experiment by running this python script using **new_var** as argument by setting `new_var = 10000`:

```python
import SetProxPythonPath
from proxtoolbox.experiments import exper

EXP = exper(new_var = 1000)
EXP.run()
EXP.show()
```
