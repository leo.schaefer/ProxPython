"""
Provides the function `createDocs()` that will execute that will execute on tagged commits.
"""
import os
import sys

def createDocs():
    os.chdir(os.path.dirname(__file__))
    print("creating docs...")
    make_api()
    print("creating html docs...")
    make_docs("html")
    print("converting tutorial to .tex")
    make_tutorial()


def make_api():
    cmd = "sphinx-apidoc -M -f -o Manual/src ../proxtoolbox ../proxtoolbox/Algorithms_old ../proxtoolbox/Problems_old ../proxtoolbox/ProxOperators_old ../proxtoolbox/algorithms/samsara/debug"
    os.system(cmd)

def make_docs(format):
    if format == "html":
        cmd = "sphinx-build -b html -d _build/doctrees  . _build/html"
    else:
        raise NotImplementedError("Format "+str(format)+" not supported")
    os.system(cmd)

def make_tutorial():
    cmd = "sphinx-build -b latex Tutorial _build/tutorial"
    os.system(cmd)