#SG
# new Cone_and_sphere code

from proxtoolbox.algorithms.algorithm import Algorithm
from numpy import exp
from proxtoolbox.utils.cell import Cell, isCell

class DRAP(Algorithm):
    """
    A type of hybrid Douglas-Rachford and Alternating projections 
    proposed by Nguyen H. Thao, "A convergent relaxation of the 
    Douglas--Rachford algorithm", Comput. Optim. Appl., 70
    (2018), pp. 841--863.    

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Aug 18, 2017.    
    """
    def evaluate(self, u):
        """
        Update of the Relaxed Averaged Alternating
        Reflection algorithm.
       
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            The current iterate.
               
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The new iterate (same type as input parameter `u`).
        """

        lmbd = self.computeRelaxation()  
        if not isCell(u):    
            tmp1 = self.prox2.eval(u)
            tmp2 = lmbd*(tmp1 - u)
            tmp3 = self.prox1.eval(tmp1 + tmp2)
            u_new = tmp3 - tmp2
        else:
            tmp1 = self.prox2.eval(u)
            tmp2 = Cell(len(u))
            tmp3 = Cell(len(u))
            u_new = Cell(len(u))
            for j in range(len(u)):
                tmp2[j] = lmbd*(tmp1[j] - u[j])
                tmp3[j] = tmp1[j] + tmp2[j]
            tmp3 = self.prox1.eval(tmp3)
            # update
            for j in range(len(u)):
                u_new[j] = tmp3[j] - tmp2[j]
        return u_new

    def getDescription(self):
        return self.getDescriptionHelper("\\lambda", self.lambda_0, self.lambda_max)