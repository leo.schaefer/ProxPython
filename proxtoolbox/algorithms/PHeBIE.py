from proxtoolbox.algorithms.algorithm import Algorithm
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
from numpy import ones


class PHeBIE(Algorithm):
    """
    Proximal Heterogenious Block Implicit-Explicit (PHeBIE) 
    minimzation algorithm as proposed in the paper
    "Proximal Heterogeneous Block Implicit-Explicit Method and
    Application to Blind Ptychographic Diffraction Imaging",
    R. Hesse, D. R. Luke, S. Sabach and M. K. Tam, 
    SIAM J. on Imaging Sciences, 8(1):426--457 (2015).
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Aug. 31, 2017.    
    """
    def __init__(self, experiment, iterateMonitor, accelerator):
        super(PHeBIE, self).__init__(experiment, iterateMonitor, accelerator)
        
        # Sets the PHeBIE block stepsizes
        if hasattr(experiment, 'beta'):
            self.beta = experiment.beta
        else:
            self.beta = ones(experiment.nProx-1) # TODO check if correct

        # the stepsize parameter gamma is only needed for the analysis.
        #For the implmentation, this can be taken to be machine zero.
        if hasattr(experiment, 'gamma'):
            self.gamma = experiment.gamma
        else:
            self.gamma = 1e-30

        # instantiate explicit prox operators
        self.explicit = []
        for prox_item in experiment.explicit:
            if isinstance(prox_item, str):
                proxClass = getattr(proxoperators, prox_item)
            else:
                proxClass = prox_item
            prox = proxClass(experiment)
            self.explicit.append(prox)

    def evaluate(self, u):
        """
        Update for the PHeBIE algorithm

        Parameters
        ----------
        u : a cell whose cell elements are the blocks
            The current iterate. 
               
        Returns
        -------
        u_new : type ???
            The new iterate.
        """

        if self.debug:
            print("Iteration: ", self.iter)
        u_new = u.copy()

        # Loop through the block-wise implicit-explicit update steps.
        K = len(self.proxOperators)
        for s in range(K):
            u_new[s] = self.explicit[s].eval(u_new, s) # explicit prox evaluation
            u_new[s] = self.proxOperators[s].eval(u_new[s], s) # implicit prox evauation

        # apply first order acceleration if required
        if self.accelerator is not None:
            u_new = self.accelerator.evaluate(u_new, self)

        return u_new


