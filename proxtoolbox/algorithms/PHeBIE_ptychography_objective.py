
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy.linalg import norm


class PHeBIE_ptychography_objective:
    """
    objective function for the PHeBIE algorithm applied to the 
    ptychography problem
       
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Feb. 12, 2019.    
    """

    def __init__(self, experiment):
        if hasattr(experiment, 'norm_data'):
            self.norm_data = experiment.norm_data
        else:
            self.norm_data = 1.0
        self.positions = experiment.positions



    def calculateObjective(self, alg):
        """
        Evaluate PHeBIE objective function
            
        Parameters
        ----------
        alg : algorithm instance
            The algorithm that is running
         
        Returns
        -------
        objValue : real
            The value of the objective function 
        """
        objValue = 0
        u = alg.u_new
        normM = self.norm_data
        self.rangeNx = np.arange(alg.Nx, dtype=np.int)
        self.rangeNy = np.arange(alg.Ny, dtype=np.int)
        if isCell(u[2]):
            for jj in range(len(u[2])):
                indy = (self.positions[jj,0] + self.rangeNy).astype(int)
                indx = (self.positions[jj,1] + self.rangeNx).astype(int)
                objValue += (norm(u[0]*u[1][indy,:][:,indx] - u[2][jj], 'fro')/normM)**2
        else:
            _m, _n, p, _q = size_matlab(u[2])
            for jj in range(p):
                indy = (self.positions[jj,0] + self.rangeNy).astype(int)
                indx = (self.positions[jj,1] + self.rangeNx).astype(int)
                objValue += (norm(u[0]*u[1][indy,:][:,indx] - u[2][:,:,jj], 'fro')/normM)**2
        return objValue


