
"""
This package contains the different algorithms that can 
be used when running the various experiments contained in
the proxtoolbox. It contains the abstract Algorithm class and
various concrete classes. 
"""

from .AP import *
from .DRAP import *
from .RAAR import *
from .RRR import *
from .HPR import *
from .CP import *
from .AvP import *
from .QNAvP import *
from .DRl import *
from .CDRl import *
from .CADRl import *
from .PHeBIE import *
from .iterateMonitor import *
from .feasibilityIterateMonitor import *
from .PHeBIE_IterateMonitor import*
from .CT_IterateMonitor import*
from .ADMM import *
from .ADMM_IterateMonitor import *
from .ADMM_PhaseLagrangeUpdate import *
from .ADMM_phase_indicator_objective import *
from .AvP2 import *
from .AvP2_IterateMonitor import *
from .GenericAccelerator import *
from .DyRePr import*
from .PHeBIE_ptychography_objective import *
from .PHeBIE_phase_objective import *
from .Wirtinger import *
from .Wirt_IterateMonitor import *
from .NSLS_sourceloc_objective import *

def __addCls__(alg):
    """
    Add a class to the namespace of this module.
 
    Parameters
    ----------
    cls : class
        The class to be added to this module.
    """
    name = alg.__name__
    if(name in globals()):
        print("Name "+str(name)+" already exists. The old algorithm will be OVERWRITTEN!")
    globals()[name] = alg

def addExternalAlgorithm(algorithm):
    """
    Add the algorithm to the known list. It will become available by using the class name as algorithm_name.
    This is not required for algorithms that are already imported to this module.
 
    Parameters
    ----------
    algorithm : a class extending the Algorithm class
        Algorithm to be added to the list.
    """
    __addCls__(algorithm)

def addExternalIterateMonitor(monitor):
    """
    Add the iterate monitor to the known list. It will become available by using the class name as iterate_monitor_name.
    This is not required for iterate monitors that are already imported to this module.
    
    This function is equivalent to addExternalAlgorithm 

    Parameters
    ----------
    monitor : a class extending the IterateMonitor class
        IterateMonitor to be added to the list.
    """
    __addCls__(monitor)

def addExternalAccelerator(accelerator):
    """
    Add the accellerator to the known list. It will become available by using the class name as accelerator_name.
    This is not required for accelerator that are already imported to this module.
    
    This function is equivalent to addExternalAlgorithm 

    Parameters
    ----------
    accelerator : class
        Accelerator to be added to the list.
    """
    __addCls__(accelerator)