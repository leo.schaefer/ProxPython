from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox.utils.mypoissonrnd import mypoissonrnd
from proxtoolbox.utils.gaussian import gaussian
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.graphics import addColorbar


#for downloading data
import proxtoolbox.utils.GetData as GetData

from proxtoolbox.folders import InputFolder

import numpy as np
from numpy import exp, sqrt, log2, log10, ceil, floor, unravel_index, argmax, zeros
from scipy.io import loadmat
from numpy.fft import fftshift, ifft2, fft2

import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class CDI_Experiment(PhaseExperiment):
    """
    CDI experiment class
    """

    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name': 'CDI',
            'object': 'nonnegative',
            'constraint': 'nonnegative and support',
            'Nx': 128,
            'Ny': 128,
            'Nz': 1,
            'sets': 10,
            'farfield': True,
            'MAXIT': 6000,
            'TOL': 1e-8,
            'lambda_0': 0.5,
            'lambda_max': 0.50,
            'lambda_switch': 30,
            'data_ball': .999826e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'rotate': False,
            'verbose': 0,
            'graphics': 1,
            'anim': False,
            'debug': True
        }
        return defaultParams

    def __init__(self,
                 warmup_iter=0,
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(CDI_Experiment, self).__init__(**kwargs)

        # do here any data member initialization
        self.warmup_iter = warmup_iter

        # the following data members are set by loadData()
        self.magn = None
        self.farfield = None
        self.data_zeros = None
        self.support_idx = None
        self.abs_illumination = None
        self.supp_phase = None

    def loadData(self):
        """
        Load CDI dataset. Create the initial iterate.
        """

        # make sure input data can be found, otherwise download it
        GetData.getData('Phase')
        if not self.silent:
            print('Loading data file CDI_intensity')
        f = loadmat(InputFolder / 'Phase/CDI_intensity.mat')
        # diffraction pattern
        dp = f['intensity']['img'][0, 0]
        orig_res = max(dp.shape[0], dp.shape[1])  # actual data size
        step_up = ceil(log2(self.Nx) - log2(orig_res))
        workres = 2**(step_up) * 2**(floor(log2(orig_res)))  # desired array size
        N = int(workres)

        # center data and use region of interest around center
        # central pixel
        # find max data value
        argmx = unravel_index(argmax(dp), dp.shape)

        Xi = argmx[0] + 1
        Xj = argmx[1] + 1

        # get desired roi:
        # necessary conversion
        Di = N/2 - (Xi-1)
        Dj = N/2 - (Xj-1)

        # roi around central pixel
        Ni = 2*(Xi + Di*(Di < 0) - 1)
        Nj = 2*(Xj + Dj*(Dj < 0) - 1)

        tmp = zeros((N, N))
        tmp[int(Di*(Di > 0)):int(Di*(Di > 0) + Ni), int(Dj*(Dj > 0)):int(Dj*(Dj > 0) + Nj)] \
                = dp[int(Xi - Ni/2) - 1:int(Xi + Ni/2 - 1), int(Xj - Nj/2) - 1:int(Xj + Nj/2 - 1)]

        M = (fftshift(tmp))**(.5)
        # M=tmp.^(.5)
        ## define support
        DX = np.ceil(N / 7)
        S = zeros((N, N))
        S[int(N/4 + 1 + DX) - 1:int(3/4 * N - 1 - DX), int(N/4 + 1 + DX) - 1:int(3/4*N - 1 - DX)] = 1

        self.magn = 1  # magnification factor
        self.farfield = True
        if not self.silent:
            print('Using farfield formula.')

        self.rt_data = Cell(1)
        self.rt_data[0] = M
        # standard for the main program is that 
        # the data field is the magnitude SQUARED
        # in Luke.m this is changed to the magnitude.
        self.data = Cell(1)
        self.data[0] = M**2
        self.norm_rt_data = np.linalg.norm(ifft2(M), 'fro')
        self.data_zeros = np.where(M == 0)
        self.support_idx = np.nonzero(S)
        self.sets = 2

        # use the abs_illumination field to represent the 
        # support constraint.
        self.abs_illumination = S
 
        # initial guess
        tmp_rnd = (np.random.rand(N, N)).T # to match Matlab
        self.u0 = S * tmp_rnd
        self.u0 = self.u0 / np.linalg.norm(self.u0, 'fro') * self.norm_rt_data

        self.dp = dp # store dp for display later

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(CDI_Experiment, self).setupProxOperators()  # call parent's method

        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'

        # remark: self.farfield is always true (set in data processor)
        # self.proxOperators already contains a prox operator at slot 0.
        # Here, we add the second one.
        if self.constraint == 'phaselift':
            self.proxOperators.append('P_Rank1')
        elif self.constraint == 'phaselift2':
            self.proxOperators.append('P_rank1_SR')
        else:
            self.proxOperators.append('Approx_Pphase_FreFra_Poisson')
        self.nProx = self.sets

    def show(self):
        """
        Generate graphical output from the solution
        """

        # display plot of far field data and support constraint
        # figure(123)
        f, (ax1, ax2) = subplots(1, 2,
                                 figsize=(self.figure_width, self.figure_height),
                                 dpi=self.figure_dpi)
        im = ax1.imshow(log10(self.dp + 1e-15))
        addColorbar(im)
        ax1.set_title('Far field data')
        im = ax2.imshow(self.abs_illumination)
        ax2.set_title('Support constraint')
        plt.subplots_adjust(wspace=0.3)  # adjust horizontal space (width)
        # between subplots (default = 0.2)
        f.suptitle('CDI Data')

        # call parent to display the other plots
        super(CDI_Experiment, self).show()
