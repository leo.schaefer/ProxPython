
from proxtoolbox.experiments.phase.phaseExperiment import PhaseExperiment
from proxtoolbox.utils.mypoissonrnd import mypoissonrnd
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.graphics import addColorbar

#for downloading data
import proxtoolbox.utils.GetData as GetData

from proxtoolbox.folders import InputFolder

import numpy as np
from numpy import exp, sqrt, log2, log10, ceil, floor, unravel_index, argmax, zeros
from scipy.io import loadmat
from numpy.fft import fftshift, ifft2, fft2

import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure


class Krueger_Experiment(PhaseExperiment):
    '''
    Krueger experiment class
    '''
 
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name' : 'Krueger',
            'object': 'phase',
            'constraint': 'phase on support',
            'noise': None,
            'snr': 10e2,
            'farfield': False,
            'MAXIT': 1000,
            'TOL': 5e-7,
            'lambda_0': 0.05,
            'lambda_max': 0.05,
            'lambda_switch': 4,
            'data_ball': 1e-0,
            'diagnostic': True,
            'iterate_monitor_name': 'FeasibilityIterateMonitor',
            'verbose': 0,
            'graphics': 1,
            'anim': False
        }
        return defaultParams
    

    def __init__(self, 
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(Krueger_Experiment, self).__init__(**kwargs)

        # do here any data member initialization
        self.TOL2 = 1e-14*self.data_ball
        self.farfield = None
        self.fresnel_nr = None
        self.magn = None
        self.FT_conv_kernel = Cell(1)
        self.beam = Cell(1)

        self.data_zeros = Cell(1)

        self.abs_illumination = None
        self.support_idx = None
        self.supp_phase = None


    def loadData(self):
        """
        Load Krueger dataset. Create the initial iterate.
        """
        
        #make sure input data can be found, otherwise download it
        GetData.getData('Phase')

        # Sample: NTT-AT, ATN/XREESCO-50HC
        # 500 nm thick Ta structure: amplitude transmission 0.9644,
        # phase shift 0.4rad at E = 17.5 keV
        # parameters see below

        # empty waveguide (WG) beam
        WG = loadmat(InputFolder / 'Phase/WG_beam.mat')
        WG = WG['WG']

        # hologram
        if not self.silent:
            print('Loading data hologram_not-normalized.mat')
        I_exp = loadmat(InputFolder / 'Phase/hologram_not-normalized.mat')
        I_exp = I_exp['I_exp']

        I_exp[np.isnan(I_exp)] = 1

        # The following is NOT used because it is not clear how the ``normalized" hologram
        # is obtained.  I suspect it is obtained by simply dividing out the empty beam
        # data in the obervation plane.  But this is incorrect.  Despite the 
        # efforts of the Hohage team to get around this error by ad hoc regularization, 
        # we take the following exact approach:  back propagate the empty beam, and 
        # divide this from the unknown object in the object plane.  This approach is true 
        # to the imaging model and does not introduce any approximations. It does, however, 
        # expose the fact that we do not know the phase of the beam.   
        # clear I_exp
        # load 'data/hologram.mat'
        ## single image reconstruction

        # number of pixels
        Ny = I_exp.shape[0]
        Nx = I_exp.shape[1]
        self.Ny = Ny
        self.Nx = Nx

        ##########################
        # Experimental parameters
        ##########################

        # energy in keV
        E = 17.5

        # wavelength [m]
        lambd = 12.398 / E * 1E-10
        k = 2 * np.pi / lambd

        # distance source-sample [m]
        z1 = 7.48e-3

        # distance sample-detector [m]
        z2 = 3.09

        # effective distance of detector
        z_eff = z1 * z2 / (z1 + z2)

        # effective magnification factor
        M = (z1 + z2) / z1

        # pixel size in detector plane
        dx_det = 55e-6
        dy_det = 55e-6

        # magnified coordinate system in detector plane
        # [X_det,Y_det] = meshgrid(dx_det*[0:1:Nx-1],dy_det*[0:1:Ny-1]);

        # demagnified pixel size in sample plane
        dx = dx_det / M
        self.dx = dx
        dy = dy_det / M
        self.dy = dy
        
        # coordinate system in sample plane
        # [X,Y] = meshgrid(dx*((1:1:Nx)-floor(Nx/2)-1),dy*((1:1:Ny)-floor(Ny/2)-1));

        # magnified coordinate system in detector plane
        # [X_det,Y_det] = meshgrid(dx_det*((1:1:Nx)-floor(Nx/2)-1),dy_det*((1:1:Ny)-floor(Ny/2)-1));

        # grid conversion in q-space
        dqx = 2 * np.pi / (Nx * dx)
        dqy = 2 * np.pi / (Ny * dy)

        [Qx, Qy] = np.meshgrid(dqx * (range(1, Nx + 1) - np.floor(Nx / 2) - 1),
                               dqy * (range(1, Ny + 1) - np.floor(Ny / 2) - 1))

        # Prepare data for ProxToolbox:
        # Fresnel propagator:
        self.farfield = False
        #config['Nx'] = Nx
        #config['Ny'] = Ny
        self.fresnel_nr = 1 * 2 * np.pi * Nx  # Fresnel number
        self.magn = 1  # magnification factor (should this be M above, or
        # is it okay since the demagnified pixel size is used?)

        kappa = np.sqrt(k**2 - (np.square(Qx) + np.square(Qy)))
        self.FT_conv_kernel[0] = fftshift(exp(-1j * kappa * z_eff))
        self.beam[0] = abs(ifft2(fft2(sqrt(WG * 7.210116465530644e-04)) / self.FT_conv_kernel[0]))
        # the scaling factor of the waveguide array above was 
        # reverse engineered from the scaling that was apparently
        # used in the preparation of the data in the file hologram.mat
        # I don't want to use this hologram as the data, preferring instead
        # to keep the experimental data as close to the actual observation
        # as possible.  Also, I am disagreeing with what my colleagues in 
        # physics think is the correct way to compensate for a structured 
        # empty beam.  According to my reverse engineering, it appears that 
        # they have divided the EMPTY BEAM MEASUREMENT WG from the object
        # IN THE OBJECT PLANE.  It is true that you need to do the empty beam 
        # correction in the object plane, though not with the empty beam, but 
        # rather the reverse propagated empty beam, as I have done above. The
        # reason being that the empty beam is measured in the measurement plane, 
        # so it does not make sense to divide the object&beam in the object 
        # plane by the empty beam in the measurement plane -  you should divide
        # by the empty beam propagated back to the object plane.  The difficulty in 
        # this is that we do not know the phase of the empty beam.  The above 
        # formulation sets the phase of the beam to zero at the object plane. 
        # The beam is saved as the square root of the empty beam to conform with 
        # the projeciton operations working on the amplitude instead of the
        # magnitude.			

        # problem data
        self.data = Cell(1)
        self.data[0] = I_exp
        self.rt_data = Cell(1)
        self.rt_data[0] = sqrt(I_exp)
        #self.data_zeros = self.data == 0
        self.data_zeros[0] = np.where(self.data[0] == 0)
        self.norm_rt_data = sqrt(sum(sum(self.data[0])))
        # use the abs_illumination field to represent the 
        # support constraint.
        self.abs_illumination = np.ones(I_exp.shape)
        self.support_idx = np.nonzero(self.abs_illumination)
        self.sets = 2
        self.product_space_dimension = 1
        self.supp_phase = []

        # start values
        self.u0 = ifft2(fft2(self.rt_data[0] * self.magn) / self.FT_conv_kernel[0]) / self.beam[0]

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(Krueger_Experiment, self).setupProxOperators() # call parent's method
        
        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'
        self.proxOperators = []
        self.proxOperators.append('P_Amod')
        self.proxOperators.append('Approx_Pphase_FreFra_Poisson')
        self.sets = self.sets
        self.nProx = self.sets
  

    def show(self):

        # display data
        f, ((ax1, ax2), (ax3, ax4)) = subplots(2, 2, \
                    figsize = (self.figure_width, self.figure_height),
                    dpi = self.figure_dpi)
        self.createImageSubFigure(f, ax1, self.beam[0],
                                  'Empty beam - Detector plane')
        self.createImageSubFigure(f, ax2, self.data[0],
                                  'Uncorrected near field observation')
        self.createImageSubFigure(f, ax3, np.abs(self.u0),
                                  'Initial guess amplitude')
        self.createImageSubFigure(f, ax4, np.angle(self.u0),
                                  'Initial guess phase')
        plt.subplots_adjust(wspace = 0.3) # adjust horizontal space (width)
                                          # between subplots (default = 0.2)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height)
                                          # between subplots (default = 0.2)
        f.suptitle('Krueger Data')

        # call parent to display the other plots
        super(Krueger_Experiment, self).show()  


    def createImageSubFigure(self, f, ax, u, title = None):
        # Note: this method overides the method defined in 
        # class PhaseExperiment
        x_max = self.Nx*self.dx*1E6
        y_max = self.Ny*self.dy*1E6
        im = ax.imshow(u, extent=[0, x_max, y_max, 0], cmap='gray')
        addColorbar(im)
        ax.set_xlabel(r'$x$ [$\mathrm{\mu m}$]')         
        ax.set_ylabel(r'$y$ [$\mathrm{\mu m}$]')         
        if title is not None:
            ax.set_title(title)

