
# pylint: disable=no-member # for dynamically created variables
# pylint: disable=access-member-before-definition # for dynamically created variables
from proxtoolbox.experiments.experiment import Experiment
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.folders import InputFolder
from proxtoolbox.utils.loadMatFile import loadMatFile
from proxtoolbox.utils.graphics import addColorbar
from proxtoolbox.experiments.ptychography.ptychographyUtils import circ, \
        heaviside, ellipsis, prop_nf_pa, im2hsv

#for downloading data
import proxtoolbox.utils.GetData as GetData

import numpy as np
from numpy import exp, sqrt, log2, log10, ceil, floor, unravel_index, argmax, zeros
from numpy.random import random_sample
from numpy.linalg import norm

from scipy.io import loadmat
import h5py

from  math import atan, atan2

# from scipy.misc import imrotate # requires library PIL to be installed, use rotate instead
from scipy.ndimage.interpolation import rotate

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import subplots, show, figure

from pathlib import Path
import copy


class PtychographyExperiment(Experiment):
    '''
    Ptychography experiment class
    '''
 
    @staticmethod
    def getDefaultParameters():
        defaultParams = {
            'experiment_name' : 'Ptychography',
            'object': 'complex',
            'constraint': 'amplitude only',
            'data_dir': InputFolder /'Ptychography',
            'datafile': 'data_NTT_01_26210_192x192',
            'farfield': True,
            'noise': None,
            'poissonfactor': 5,
            'plot': True,
            'switch_probemask': True,
            'rmsfraction': 0.5,
            'probe_mask_gamma': 1,
            'scan_type': '', # possible values 'round_roi' or leave '' for raster
            'switch_object_support_constraint': True, 
            'probe_guess_type': 'circle',
            'object_guess_type': 'random',
            'warmup': True,
            'warmup_in': 'Wilke2_warmup_in',
            'algorithm': 'PHeBIE',
            'MAXIT': 500,
            'TOL': -1,
            'TOL2': 1e-4,
            'lambda_0': 0.05,
            'lambda_max': 0.05,
            'lambda_switch': 4,
            'data_ball': 1e-30,
            'diagnostic': True,
            'iterate_monitor_name': 'PHeBIE_IterateMonitor',
            'verbose': 0,
            'graphics': 1,
            'anim': False
        }
        return defaultParams

    defaultWarmupParams = {
        'algorithm': 'DRl',
        'MAXIT': 2,
        'TOL': 5e-5,
        'lambda_0': 0.75,
        'lambda_max': 0.75,
        'lambda_switch': 20,
        'data_ball': 1e-30,
        'diagnostic': True,
        'rotate': False,
        'iterate_monitor_name': 'IterateMonitor',
        'verbose': 1,
        'graphics': 0,
        'anim': False
    }


    def __init__(self, 
                 data_dir = InputFolder / 'Ptychography',
                 datafile = 'data_NTT_01_26210_192x192',
                 farfield = True,   
                 poissonfactor = 5,
                 plot = True,
                 switch_probemask = True,
                 rmsfraction = 0.5,
                 probe_mask_gamma = 1,
                 scan_type = None,
                 switch_object_support_constraint = True,
                 probe_guess_type = 'circle',
                 object_guess_type = 'random',
                 warmup = True,
                 warmup_params = defaultWarmupParams,
                 ptychography_prox = 'PHeBIE_Ptwise',
                 overrelax = 1+1e-30,
                 blocking_switch = False,
                 blocking_scheme = 'divide',
                 between_blocks_scheme = 'sequential',
                 within_blocks_scheme  = 'sequential',
                 block_rows = 2,
                 block_cols = 2,
                 block_MAXIT = 1,
                 block_verbose = 0,
                 block_anim = 1,
                 RodenburgInnerIt = 1,
                 error_type = 'custom',
                 **kwargs):
        """
        """
        # call parent's __init__ method
        super(PtychographyExperiment, self).__init__(**kwargs)
        # do here any data member initialization
        self.data_dir = None if data_dir is None else Path(data_dir)
        self.datafile = datafile
        self.farfield = farfield
        self.poissonfactor = poissonfactor
        self.plot = plot
        self.switch_probemask = switch_probemask
        self.rmsfraction = rmsfraction
        self.probe_mask_gamma = probe_mask_gamma
        self.scan_type = scan_type
        self.switch_object_support_constraint = switch_object_support_constraint,
        self.probe_guess_type = probe_guess_type
        self.object_guess_type = object_guess_type
        self.warmup = warmup
        self.warmup_params = warmup_params
        self.ptychography_prox = ptychography_prox
        self.overrelax = overrelax
        self.blocking_switch = blocking_switch
        self.blocking_scheme = blocking_scheme
        self.between_blocks_scheme = between_blocks_scheme
        self.within_blocks_scheme = within_blocks_scheme
        self.block_rows = block_rows
        self.block_cols = block_cols,
        self.block_MAXIT = block_MAXIT,
        self.block_verbose = block_verbose
        self.block_anim = block_anim
        self.RodenburgInnerIt = RodenburgInnerIt
        self.error_type = error_type

        # internal data members
        self.proxSequence = None

        # TODO override iterate monitor and optimality_monitor based
        # algorithm and/or prox configuration being used
 

    def loadData(self):
        """
        Load Ptychography dataset. Create the initial iterate.
        """

        #make sure input data can be found, otherwise download it
        GetData.getData('Ptychography')

        if self.datafile is not None:
            if self.data_dir is not None:
                data_path = self.data_dir / self.datafile
            else:
                data_path = Path(self.datafile)
            if not data_path.name.endswith('.mat'):
                data_path = data_path.parent / (data_path.name + '.mat')

            if not data_path.is_file():
                print('*************************************************************************')
                print('* INPUT DATA MISSING.  Please download the ptychography input data from *')
                print('* http://vaopt.math.uni-goettingen.de/data/Ptychography.tar.gz          *')
                print('* Save and unpack the Ptychography.tar.gz datafile in the               *')
                print('*    ProxMatlab/InputData/Ptychography subdirectory                     *')
                print('*************************************************************************')
                errMsg = "File " + data_path + " was not found"
                raise IOError(errMsg)

            mat_dict = loadMatFile(data_path)
            P = mat_dict['P']
            # Make all the variables contained in P attributes of 
            # this experiment object      
            # Note: We do this for now (in the same way this was done in
            # Matlab) even if this is obviously not a good practice.
            # Indeed, this means we need to stick to the same 
            # variable names used in Matlab. Considering possible 
            # collision with names, this is sometimes desired (with data
            # and dataSq for instance). The alternative would be to store
            # P in a specific member of the experiment class 
            # and then update the required (common) variables 
            # in the experiment class.

            for var, value in P.items():
                if var != 'data_dir': # we don't want to overwrite self.data_dir
                    if var == 'lambda': # lambda is a reserved word for Python
                                        # change to 'lmbda'
                        setattr(self, 'lmbda', value)
                    else:
                        setattr(self, var, value)
            
            # Another issue is that the integer variables contained
            # in the data file are converted to float by .mat file
            # reader (the problem lies in the library h5py that provides
            # the services to read the .mat file)
            self.Nx = int(self.Nx)
            self.Ny = int(self.Ny)
            self.Nz = int(self.Nz)
            self.nx = int(self.nx)
            self.ny = int(self.ny)
            self.N_pie = int(self.N_pie)
 
            if self.datafile == 'data_NTT_01_26210_192x192':
                reconst_file = 'reconstruction_data_NTT_01_26210_192x192.mat'
                if self.data_dir is not None:
                    file_path = self.data_dir / reconst_file
                else:
                    file_path = reconst_file
                reconst_dict = loadMatFile(file_path)
                QQ = reconst_dict['P']
                pos_file = 'positions_NTT_01_26210.mat'
                if self.data_dir is not None:
                    file_path = self.data_dir / pos_file
                else:
                    file_path = pos_file
                pos_dict = loadMatFile(file_path)
                positions = pos_dict['positions']
                positions[:,1] = positions[:,1]/self.d1x # horizontal axis
                positions[:,0] = positions[:,0]/self.d1y # vertical axis
                positions[:,0] = positions[:,0] - np.min(positions[:,0])
                positions[:,1] = positions[:,1] - np.min(positions[:,1])
                self.positions = np.round(positions)
                self.sample_plane = QQ['object']
                probe = QQ['Probe'] # no need to keep it as a data member
                self.trans_min_true = 0
                self.trans_max_true = 1
            if self.debug:
                # The following reduces the size of the problem
                # by removing some of the data so that you can
                #  debug on your laptop.
                keepPos = np.sum(((self.positions)<30),1)==2
                self.positions = self.positions[keepPos,:]
                self.N_pie = np.sum(keepPos)
                s = self.I_exp.shape
                tmp = self.I_exp.reshape((s[0],s[1],s[2]*s[3]), order='F')
                self.I_exp = tmp[:,:,keepPos]
                s = self.fmask.shape
                tmp = self.fmask.reshape((s[0],s[1],s[2]*s[3]), order='F')
                self.fmask = tmp[:,:,keepPos]
                self.ny = np.sum(keepPos)
                self.nx = 1
        # else: generate data TODO    

        # Process the data (based on Paer's data processor).

        # Average total intensity in experimental diffraction pattern
        I_sum = np.sum(np.sum(self.I_exp,axis=1),axis=0)
        I_avg = np.mean(I_sum)
        if not self.silent:
            print("Average intensity of single frame is {:.2e} photons".format(I_avg))

        # Maximum average intensity in a single experimental frame
        I_max_avg = np.amax(I_sum) / (self.Nx*self.Ny)

        # Generate cell array of normalized intensities
        # Normalization such, that for I = I_max (as matrix) the average pixel
        # intensity is 1 and total is Nx*Ny, for all other I the values are lower
        self.data = Cell(self.N_pie)
        self.data_sq = Cell(self.N_pie)
        self.data_zeros = Cell(self.N_pie)
        for j in range(self.N_pie):
            self.data[j] = np.zeros((self.I_exp.shape[0], self.I_exp.shape[1]))
            self.data_sq[j] = self.data[j].copy()
        
        # Specimen plane (E1)/ probe plane (as long as direct propagation is used) -
        rangeNx = np.arange(self.Nx, dtype=np.int)
        rangeNy = np.arange(self.Ny, dtype=np.int)
        X_1, Y_1 = np.meshgrid((rangeNx-(self.Nx//2))*self.d1x,
                               (rangeNy-(self.Ny//2))*self.d1y)
 
        if not hasattr(self, 'probe_mask_gamma'):
            self.probe_mask_gamma = 1
        
        # Relative radius of ideal circular probe mask with respect to radius of
        # pinhole used for initial simulation of the probe function
        self.R = 2.2E-6
        beta_sam = 0.9*self.Nx*self.d1x/2/self.R*self.probe_mask_gamma
        self.beta_sam = beta_sam

        if self.switch_probemask:
            self.Probe_mask = circ(X_1, Y_1, 0, 0, beta_sam*self.R)
        else:
            self.Probe_mask = np.ones(probe.shape)

        #  Generate noise
        if self.noise == 'poisson':
            raise NotImplementedError('Addition of noise is not implemented')
            print('Adding Poisson noise to measurement data...')
            for i in range(self.N_pie):
                pass
                # TODO we'll need to reshape the data
                # Matlab code:
                # P.I_exp(:,:,i) = arrayfun(@(u) (u + 10^(PoissonRan(P.poissonfactor))), P.I_exp(:,:,i));
        else:
            print('No noise added to measurement data...')

        cx = self.I_exp.shape[1]//2
        cy = self.I_exp.shape[0]//2

        # Removal of residual noise in the data is done here.
        ind = 0
        bs_mask = None
        if hasattr(self, 'bs_mask'):
            bs_mask = self.bs_mask
        fmask = None
        if hasattr(self, 'fmask'):
            fmask = self.fmask
        for n_slow in range(self.ny):
            for n_fast in range(self.nx):
                # check if semi-transparent central stop was used.
                # Scale intensity accordingly
                if bs_mask is not None:
                    dat = self.I_exp[:,:,n_slow,n_fast]
                    dat[bs_mask] *= self.bs_factor
                    self.I_exp[:,:,n_slow,n_fast] = dat
                
                # Save time and get experimental data into 
                # fft2 non-centered system
                dat = np.rot90(np.roll(np.roll(self.I_exp[:,:,n_slow,n_fast], \
                               -cy, axis=0), -cx, axis=1), 2)
                
                # Normalization such that average intensity of
                # data is on the order of 1
                self.data[ind] = np.sqrt(dat/I_max_avg)
                self.data_sq[ind] = dat/I_max_avg
                self.data_zeros[ind] = np.where(self.data[ind] == 0)
                
                #check for pixelmask
                if fmask is not None:
                    tmp_mask = np.rot90(np.roll(np.roll( \
                         fmask[:,:,n_slow,n_fast], -cy, axis=0), -cx, axis=1) ,2)
                    fmask[:,:,n_slow,n_fast] = tmp_mask
                ind += 1

        # reshape fmask 
        if fmask is not None and fmask.ndim > 3:
            s = fmask.shape
            fmask = fmask.reshape((s[0],s[1],s[2]*s[3]), order='F')
            self.fmask = fmask
            
        guess_value = 1000

        # Inital guess for the probe
        probe_guess = None
        if self.probe_guess_type == 'exact':
            probe_guess = probe.copy()
        elif self.probe_guess_type == 'exact_amp':
            probe_guess = np.abs(probe)
        elif self.probe_guess_type == 'circle':
            probe_guess = np.zeros(probe.shape)
            radius = 13.0/256.0 * probe.shape[0]
            x_c = probe.shape[0]/2
            y_c = probe.shape[1]/2
            for i in range(probe.shape[0]):
                for j in range(probe.shape[1]):
                    if sqrt((i+1-x_c)**2 + (j+1-y_c)**2) < radius:
                        probe_guess[i,j] = guess_value
        elif self.probe_guess_type == 'robin_initial':
            # TODO not tested
            a = .2E-6
            b = .2E-6
            alpha = 0
            X_1, Y_1 = np.meshgrid((rangeNx-(self.Nx//2))*self.d1x,
                                   (rangeNy-(self.Ny//2))*self.d1y)
            probe_guess = np.abs(rotate(ellipsis(X_1, Y_1, 0, 0, a, b), 0))
            probe_guess = prop_nf_pa(probe_guess, self.lmbda, self.z01,
                                     self.d1x, self.d1y)
            probe_guess /= norm(probe_guess,'fro') / sqrt(np.size(probe_guess))
        elif self.probe_guess_type == 'ones':
            probe_guess = np.ones_like(probe)
        else: # TODO implement missing cases
            errMsg = "Probe guess type " + self.probe_guess_type \
                      + " is not yet implemented"
            raise NotImplementedError(errMsg)

        # Initial guess for the object
        object_guess = None
        if self.object_guess_type == 'exact':
            object_guess = self.sample_plane.copy()
        elif self.object_guess_type == 'ones':
            object_guess = np.ones_like(self.sample_plane) / guess_value
        elif self.object_guess_type == 'constant':
            object_guess = np.ones_like(self.sample_plane) / guess_value
        elif self.object_guess_type == 'exact_perturbed':
            object_guess = self.sample_plane + 2e-1 * \
                            (random_sample(self.sample_plane.shape)-0.5)
        elif self.object_guess_type == 'random':
            #np.random.seed(self.run_number) 
            object_shape = self.sample_plane.shape
            tmp_rnd1 = random_sample(object_shape).T # transpose to match Matlab
            tmp_rnd2 = random_sample(object_shape).T
            object_guess = (1.0/guess_value) * \
                            tmp_rnd1 * \
                            np.exp(1j*(2*np.pi*tmp_rnd2 - \
                                   np.pi)/2)
        else:
            errMsg = "Object guess type " + self.object_guess_type \
                      + " is not yet implemented"
            raise NotImplementedError(errMsg)

        # Legacy variables
        self.product_space_dimension = self.N_pie
        self.norm_data = 1

        # factor for probe
        cfact = 0.0001*self.ny*self.nx
        self.cfact = cfact

        # Compute object support constraint (using initial probe guess)
        if self.probe_guess_type == 'exact':
            probe_guess = np.zeros_like(probe)
            probe_size = probe.shape
            radius = 13/256 * probe_size[0]
            x_c = probe_size[0]/2
            y_c = probe_size[1]/2
            for i in range(probe_size[0]):
                for j in range(probe_size[1]):
                    if sqrt((i - x_c)**2 + (j - y_c)**2) < radius:
                        probe_guess[i,j] = guess_value
  
        low = 0.1
        object_support = np.zeros(self.sample_plane.shape)
        if self.probe_guess_type == 'robinGaussian':
            probe_mask = self.Probe_mask
        else:
            probe_mask_indices = (np.abs(probe_guess)**2)>low
            probe_mask = probe_mask_indices.astype(object_support.dtype)
        for pos in range(self.N_pie):
            indy = (self.positions[pos,0] + rangeNy).astype(int)
            indx = (self.positions[pos,1] + rangeNx).astype(int)
            for y in range(self.Ny):
                object_support[indy[y],indx] += probe_mask[y,:]

        self.object_support = (object_support > 0).astype(object_support.dtype)

        object_guess *= self.object_support
        self.sample_plane *= self.object_support

        if self.switch_probemask:
            probe *= self.Probe_mask

        if self.switch_object_support_constraint:
            object_guess *= self.object_support
        if self.switch_probemask: # Already done above, so this is likely a mistake (in Matlab)
            probe *= self.Probe_mask

        # Generate initial guess for exit wave functions
        phi_type = probe_guess.dtype
        if np.iscomplexobj(object_guess):
            phi_type = object_guess.dtype
        phi = np.ones((self.Nx, self.Ny, self.N_pie), dtype=phi_type)
        for pos in range(self.N_pie):
            indy = (rangeNy + self.positions[pos,0]).astype(int)
            indx = (rangeNx + self.positions[pos,1]).astype(int)
            phi[:,:,pos] = probe_guess * object_guess[indy,:][:,indx]

        # set the initial iterate
        self.u0 = Cell(3)
        self.u0[0] = probe_guess 
        self.u0[1] = object_guess
        self.u0[2] = phi
       
        # remove attributes (to follow Matlab code)
        # that will no longer be used
        delattr(self, 'I_exp')
        

    def setupProxOperators(self):
        """
        Determine the prox operators to be used for this experiment
        """
        super(PtychographyExperiment, self).setupProxOperators() # call parent's method
        
        self.nProx = 3
        self.propagator = 'Propagator_FreFra'
        self.inverse_propagator = 'InvPropagator_FreFra'
        self.explicit = []
        self.proxOperators = []
        self.productProxOperators = []

        if self.ptychography_prox == 'PHeBIE_Ptwise':
            self.n_product_Prox = self.product_space_dimension
            for _j in range(self.n_product_Prox):
                self.productProxOperators.append('Approx_Pphase_FreFra_Poisson')
            # prox 0
            self.explicit.append('Explicit_ptychography_scan_farfield_probe_ptwise')
            self.proxOperators.append('P_ptychography_scan_farfield_probe')
            # prox 1
            self.explicit.append('Explicit_ptychography_scan_farfield_object_ptwise')
            self.proxOperators.append('P_supp_amp_band')
            # prox 3
            self.explicit.append('Explicit_ptychography_image')
            self.proxOperators.append('Prox_product_space')
            self.proxSequence = [1, 2, 3]
        else:
            errMsg = "Prox operator configuration " + self.ptychography_prox \
                      + " is not yet implemented"
            raise NotImplementedError(errMsg)

        self.optimality_monitor = 'PHeBIE_ptychography_objective'

 
        '''
        switch input.ptychography_prox
            case 'PHeBIE'
                input.iterate_monitor = 'PHeBIE_iterate_monitor';
                input.optimality_monitor = 'PHeBIE_ptychography_objective';
                input.Explicit{3}='Explicit_ptychography_image';
                input.Prox{3} = 'Prox_product_space';
                input.n_product_Prox=input.product_space_dimension;
                n_product_Prox=input.n_product_Prox;
                input.product_Prox=cell(1, n_product_Prox);
                for j=1:input.n_product_Prox
                    input.product_Prox{j} = 'Approx_Pphase_FreFra_Poisson';
                end
                
                input.Explicit{1}='Explicit_ptychography_scan_farfield_probe';  %pairs with the Implicit/prox mappings below
                input.Prox{1} = 'P_ptychography_scan_farfield_probe';
                input.Explicit{2}='Explicit_ptychography_scan_farfield_object';  %pairs with the Implicit/prox mappings below
                input.Prox{2} = 'P_supp_amp_band';
                input.ProxSequence = [1 2 3];
                % below might be legacy/junk code
                if strcmp(input.method,'Nesterov_PHeBIE')
                    input.fnames = {'probe','object','phi'};
                end
            case 'PHeBIE_Ptwise'
                input.iterate_monitor = 'PHeBIE_iterate_monitor';
                input.optimality_monitor = 'PHeBIE_ptychography_objective';
                input.Explicit{3}='Explicit_ptychography_image';
                input.Prox{3} = 'Prox_product_space';
                input.n_product_Prox=input.product_space_dimension;
                n_product_Prox=input.n_product_Prox;
                input.product_Prox=cell(1, n_product_Prox);
                for j=1:input.n_product_Prox
                    input.product_Prox{j} = 'Approx_Pphase_FreFra_Poisson';
                end
                
                input.Explicit{1}='Explicit_ptychography_scan_farfield_probe_ptwise';  %pairs with the Implicit/prox mappings below
                input.Prox{1} = 'P_ptychography_scan_farfield_probe';
                input.Explicit{2}='Explicit_ptychography_scan_farfield_object_ptwise';  %pairs with the Implicit/prox mappings below
                input.Prox{2} = 'P_supp_amp_band';
                input.ProxSequence = [1 2 3];
                % below might be legacy/junk code
                if strcmp(input.method,'Nesterov_PHeBIE')
                    input.fnames = {'probe','object','phi'};
                end
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
            % the cases below have not been updated (DRL, 19.02.2019) 
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            case 'DRlPHeBIE_Ptwise'
                input.iterate_monitor = 'PHeBIE_iterate_monitor';
                input.optimality_monitor = 'PHeBIE_ptychography_objective';
                input.Prox{1} = 'P_ptychography_PHeBIE_D_ptwise';
                input.Prox{2} = 'P_ptychography_PHeBIE_phi';
                input.ProxSequence = [1 2];
                input.DRlwith = {'phi'}; %a list of file name on which reflections are to be performed with.     
            case 'Rodenburg'
                input.iterate_monitor = 'generic_iterate_monitor';
                input.optimality_monitor = '';
                input.Prox{1} = 'P_ptychography_Rodenburg';
                input.ProxSequence = [1];
            case 'Thibault'
                input.iterate_monitor = 'generic_iterate_monitor';
                input.optimality_monitor = '';
                input.Prox{1} = 'P_ptychography_Thibault_OP';
                input.Prox{2} = 'P_ptychography_Thibault_F';
                input.ProxSequence = [1 2];
                input.warmup_Prox{1} = 'P_ptychography_Thibault_O';
                input.warmup_Prox{2} = 'P_ptychography_Thibault_F';
                input.warmup_ProxSequence = [1 2];
                if ~strcmp(input.method,'RAAR_PHeBIE')||~strcmp(input.warmup_method,'RAAR_PHeBIE')
                    warning(['Method "' input.method '" is incorrect for use with Thibault. Method changed to "RAAR_PHeBIE". If you wish to use a Thibault-like method without RAAR, set ptychography_prox=Thibault_AP.'])
                    input.method = 'RAAR_PHeBIE';
                    input.warmup_method = 'RAAR_PHeBIE';
                end
                input.RAARwith = {'phi'}; %a list of file name on which reflections are to be performed with.
            case 'Thibault_fattened'
                input.iterate_monitor = 'generic_iterate_monitor';
                input.optimality_monitor = '';
                input.Prox{1} = 'P_ptychography_Thibault_OP';
                input.Prox{2} = 'P_ptychography_Thibault_F';
                input.ProxSequence = [1 2];
                input.warmup_Prox{1} = 'P_ptychography_Thibault_O';
                input.warmup_Prox{2} = 'P_ptychography_Thibault_F';
                input.warmup_ProxSequence = [1 2];
                if ~strcmp(input.method,'RAAR_PHeBIE')||~strcmp(input.warmup_method,'RAAR_PHeBIE')
                    warning(['Method "' input.method '" is incorrect for use with Thibault. Method changed to "RAAR_PHeBIE". If you wish to use a Thibault-like method without RAAR, set ptychography_prox=Thibault_AP.'])
                    input.method = 'RAAR_PHeBIE';
                    input.warmup_method = 'RAAR_PHeBIE';
                end
                input.RAARwith = {'phi'}; %a list of file name on which reflections are to be performed with.
            case 'Thibault_AP'
                input.Prox{1} = 'P_ptychography_Thibault_OP';
                input.Prox{2} = 'P_ptychography_Thibault_F';
                input.ProxSequence = [1 2];
                input.warmup_Prox{1} = 'P_ptychography_Thibault_O';
                input.warmup_Prox{2} = 'P_ptychography_Thibault_F';
                input.warmup_ProxSequence = [1 2];
            otherwise
                error('Error: Prox-operators could not be found.')
        end
        '''
  
    def retrieveProxOperatorClasses(self):
        '''
        Retrieve the Python classes corresponding to the prox operators
        defined by the setupProxOperators() method.
        
        This is a helper method called during the initialization
        process. It is overriden here to deal with an additional
        category of prox operators : explicit prox operators. 
        '''
        super(PtychographyExperiment, self).retrieveProxOperatorClasses() # call parent's method
       
        # find prox operators classes based on their name
        # and replace names by actual classes
        classes = []
        for prox in self.explicit:
            if prox != None:
                classes.append(getattr(proxoperators, prox))
        self.explicit = classes

    def initialize(self):
        """
        Initialize experiment object. 
        
        This method is overriden to instanciate the warmup algorithm
        """
        super(PtychographyExperiment, self).initialize() # call parent's method
        
        if self.warmup:
            # initialize warmup algorithm
            warmupExp = copy.copy(self) # create a copy of this experiment

            # update this copy with warmup parameters
            for var, value in self.warmup_params.items():
                if var == 'algorithm':                                    
                    setattr(warmupExp, 'algorithm_name', value)
                else:
                    setattr(warmupExp, var, value)
            
            # set-up prox operators
            warmupExp.proxOperators = []
            warmupExp.productProxOperators = []
            warmupExp.FT_conv_kernel = []
            if warmupExp.algorithm_name == 'DRl':
                warmupExp.nProx = 2
                # prox 0
                warmupExp.proxOperators.append(getattr(proxoperators,  
                                               'Prox_ptychography_ptwise_diag'))
                # prox 1
                warmupExp.proxOperators.append(getattr(proxoperators,  
                                               'Prox_ptychography_product_space'))
                # prox operators for product space
                warmupExp.n_product_Prox = self.N_pie
                warmupExp.FT_conv_kernel = Cell(self.N_pie)
                for j in range(warmupExp.n_product_Prox):
                    warmupExp.productProxOperators.append(getattr(proxoperators,  
                                                          'Approx_Pphase_FreFra_Poisson'))
                    warmupExp.FT_conv_kernel[j] = self.u0[0]
                # initial iterate
                warmupExp.u0 = Cell(2)
                warmupExp.u0[0] = self.u0[1]
                warmupExp.u0[1] = self.u0[2]
        else:
            errMsg = "Warmup algorithm  " + warmupExp.algorithm_name \
                      + " not supported yet"
            raise NotImplementedError(errMsg)
        
        # instantiate algorithm
        warmupExp.iterate_monitor_name = 'IterateMonitor'
        warmupExp.instanciateAlgorithm()

        self.warmupExp = warmupExp
            
            
    def runAlgorithm(self, u):

        if self.warmup:
            # run warmup algorithm
            if not self.silent:
                print("Running warmup algorithm " + self.warmupExp.algorithm_name)
            warmup_out = self.warmupExp.algorithm.run(self.warmupExp.u0)
            warmup_u = warmup_out['u'] 
            u0 = Cell(3)
            u0[0] = self.u0[0]
            u0[1] = warmup_u[0]
            u0[2] = warmup_u[1]
        else:
            u0 = self.u0

        if not self.silent:
            print("Running main algorithm (" + self.algorithm_name + ")")
        output = super(PtychographyExperiment, self).runAlgorithm(u0) # call parent's method
        return output
        

    def show(self):

        u0 = self.u0
        u = self.output['u']
        stats = self.output['stats']
        probe = u[0]
        obj = u[1]

        if self.truth:
            # figure 1
            #TODO
            pass

        # figure 2
        fig = plt.figure(figsize = (self.figure_width, self.figure_height),
                         dpi = self.figure_dpi)
        
        ax1 = fig.add_subplot(221)
        probe_amp_scaled = u0[0]/np.max(np.max(np.abs(u0[0])))
        probe_amp_guess = matplotlib.colors.hsv_to_rgb(im2hsv(probe_amp_scaled, 1))
        im = ax1.imshow(probe_amp_guess)
        ax1.set_title('Initial guess: probe amplitude')

        ax2 = fig.add_subplot(223)
        tmp_abs_object = np.abs(u0[1])
        object_amplitude_guess = np.log10(self.replaceZeroByMin(tmp_abs_object))
        im = ax2.imshow(object_amplitude_guess, cmap='gray')
        addColorbar(im)
        ax2.set_title('Initial guess: object amplitude')
        
        ax3 = fig.add_subplot(224)
        angle = np.angle(u0[1])
        im = ax3.imshow(angle, cmap='gray')
        addColorbar(im)
        ax3.set_title('Initial guess: object phase')

        plt.subplots_adjust(wspace = 0.3) # adjust horizontal space (width)
                                          # between subplots (default = 0.2)
        plt.subplots_adjust(hspace = 0.3) # adjust vertical space (height)
                                          # between subplots (default = 0.2)
        fig.suptitle('Ptychography Data')

        # figure 600
        fig = plt.figure(figsize = (self.figure_width, self.figure_height),
                         dpi = self.figure_dpi)
        # Object amplitude
        ax1 = fig.add_subplot(121)
        abs_obj = abs(obj)
        ampObject = np.log10(self.replaceZeroByMin(abs_obj))
        im = ax1.imshow(ampObject, cmap='gray')
        addColorbar(im)
        ax1.set_title('Best object approximation, amplitude')

        # Object phase
        ax2 = fig.add_subplot(122)
        angle = np.angle(obj)
        im = ax2.imshow(angle, cmap='gray')
        addColorbar(im)
        ax2.set_title('Best object approximation, phase')

        plt.subplots_adjust(wspace = 0.3) # adjust horizontal space (width)
                                          # between subplots (default = 0.2)

        # figure 602
        # Probe
        fig = plt.figure(figsize = (self.figure_width, self.figure_height),
                         dpi = self.figure_dpi)       
        ax1 = fig.add_subplot(111)
        scaled_probe = probe/np.max(np.max(np.abs(probe)))
        ampProbe = matplotlib.colors.hsv_to_rgb(im2hsv(scaled_probe, 1))
        im = ax1.imshow(ampProbe)
        ax1.set_title('Best probe approximation amplitude')

        # Objective function value
        # TODO
        if 'customError' in stats:
            # figure 603
            pass   

        # figure 604
        meas = zeros(self.sample_plane.shape)
        rangeNx = np.arange(self.Nx, dtype=np.int)
        rangeNy = np.arange(self.Ny, dtype=np.int)
        for pos in range(self.N_pie):
            indy = (self.positions[pos,0] + rangeNy).astype(int)
            indx = (self.positions[pos,1] + rangeNx).astype(int)
            for y in range(self.Ny):
                meas[indy[y],indx] += self.Probe_mask[y,:]

        plt.figure(figsize = (self.figure_width, self.figure_height),
                   dpi = self.figure_dpi)
        im = plt.imshow(meas, cmap='gray')
        addColorbar(im)
        plt.title('Number of measurements, pixelwise')

        # Figure 700
        changes = stats['changes']
        iterations = np.arange(1, len(changes)+1, 1)
        algo_desc = self.algorithm.getDescription()
        algo_label = "Algorithm " + algo_desc
        title = "Change in iterates, " + algo_label
        plt.figure(figsize = (self.figure_width, self.figure_height),
                   dpi = self.figure_dpi)
        plt.loglog(iterations, changes)
        #plt.semilogy(changes)
        plt.xlabel('Iteration')
        plt.ylabel('Step length')
        plt.title(title)

        # figure 701
        if 'gaps' in stats:
            gaps = stats['gaps']
            plt.figure(figsize = (self.figure_width, self.figure_height),
                                  dpi = self.figure_dpi)
            plt.semilogy(iterations, gaps)
            plt.xlabel('Iteration')
            plt.ylabel('Log of objective value') # may not be appropriate if the
                                                 # PtychgraphyIterator was not used
            title = "Objective value, " + algo_label
            plt.title(title)

        plt.show()

    def replaceZeroByMin(self, u):
        """
        Replace all the zeros in real ndarray u by the minimum value.
        Returns a new array.
        
        This is needed for plotting because the zero values
        would generate -inf when taking the log. This causes
        matplotlib to ignore those values instead of setting them to 
        the lowest value as Matlab does.
        """
        u_new = u.copy()
        zero_indices = np.where(u_new == 0)
        u_new[zero_indices] = np.inf # replace 0 by +inf
        true_min = np.min(u_new)
        u_new[zero_indices] = true_min # replace +inf by the true min
        return u_new

    def createImageSubFigure(self, f, ax, u, title = None):
        # Note: this method overides the method defined in 
        # class PhaseExperiment
        
        #x_max = self.Nx*self.dx*1E6
        #y_max = self.Ny*self.dy*1E6
        im = ax.imshow(u)
        # im = ax.imshow(u, extent=[0, x_max, y_max, 0], cmap='gray')
        addColorbar(im)
        #ax.set_xlabel(r'$x$ [$\mathrm{\mu m}$]')         
        #ax.set_ylabel(r'$y$ [$\mathrm{\mu m}$]')         
        if title is not None:
            ax.set_title(title)




