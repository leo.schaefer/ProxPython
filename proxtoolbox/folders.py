import proxtoolbox
from pathlib import Path

ProxPythonFolder = Path(proxtoolbox.__file__).absolute().parent.parent
""" Folder, that the proxtoolbox code is located in. """

InputFolder = ProxPythonFolder / "ProxPythonData"
""" Folder, that the InputData is located in. """
