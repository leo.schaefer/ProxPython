from proxtoolbox import proxoperators
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj

from proxtoolbox.utils.cell import isCell

import numpy as np
from numpy import conj, ones, zeros, exp, \
    log, tile, real, pi
# from pyfftw.interfaces.scipy_fftpack import fft2, ifft2
from numpy.fft import fft2, ifft2, fftshift, ifftshift # TODO: switch to scipy.fftpack? (is faster supposedly)


class Approx_Pphase_FreFra_Poisson(ProxOperator):
    """
    Prox operator implementing projection onto Fourier
    magnitude constraints in the near field or far field with 
    Poisson noise.  
    
    This is an approximate projector onto a ball
    around the data determined by the Kullback-Leibler divergence,
    as appropriate for Poisson noise.  The analysis of such 
    approximate projections was studied in D.R.Luke,
    "Local Linear Convergence of Approximate Projections onto
    Regularized Sets", Nonlinear Analysis, 75(2012):1531--1546.
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) around Oct 3, 2017.
    """

    def __init__(self, experiment):
        # instantiate propagator and inverse propagator;
        # retrieve classes if needed
        if isinstance(experiment.propagator, str):
            propagatorClass = getattr(proxoperators, experiment.propagator)
        else:
            propagatorClass = experiment.propagator
        if isinstance(experiment.inverse_propagator, str):
            invPropagatorClass = getattr(proxoperators, experiment.inverse_propagator)
        else:
            invPropagatorClass = experiment.inverse_propagator
        self.propagator = propagatorClass(experiment)
        self.inverse_propagator = invPropagatorClass(experiment)

        self.data = experiment.data
        self.data_sq = experiment.data_sq
        self.data_zeros = experiment.data_zeros
        self.data_ball = experiment.data_ball
        self.TOL2 = experiment.TOL2
        self.fmask = None
        if hasattr(experiment, 'fmask'):
            self.fmask = experiment.fmask

    def eval(self, u, prox_idx=None):
        """
        Projection onto Fourier magnitude constraints in the near field
        or far field with Poisson noise.
            
        Parameters
        ----------
        u : array-like
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : array_like
            The projection in the physical (time) domain in the
            same format as u.
        """

        # Propagate to the image plane:
        u_hat = self.propagator.eval(u, prox_idx)
        
        # Compute the Kullback-Leibler distance of u_hat to the data: 
        if prox_idx is None:
            j = 0
        else:
            j = min(prox_idx, len(self.data)-1)
        u_hat_sq = real(u_hat * conj(u_hat))
        # Update data_sq to prevent division by zero.
        # Note that data_sq_zeros, as defined below,
        # can be different from self.data_zeros[j]] 
        # (e.g., JWST experiment).
        data_sq = self.data_sq[j].copy()
        data_sq_zeros = np.where(data_sq == 0)
        data_sq[data_sq_zeros] = 1

        tmp = u_hat_sq / data_sq
        data_zeros = self.data_zeros[j]
        tmp[data_zeros] = 1
        u_hat_sq[data_zeros] = 0
        I_u_hat = tmp == 0
        tmp[I_u_hat] = 1
        tmp = log(tmp)
        h_u_hat = real(sum(sum(u_hat_sq*tmp + self.data_sq[j] - u_hat_sq)))
        # Now see that the propagated field is within the ball around the data (if any).  
        # If not, the projection is calculated, otherwise we do nothing. 
        if h_u_hat >= self.data_ball + self.TOL2:
            p_Mhat = magproj(self.data[j], u_hat)
            if self.fmask is not None:  # Mask in the Fourier domain:  only 2D signals.
                if not isCell(self.fmask):
                    p_Mhat = p_Mhat * self.fmask[:,:,prox_idx] \
                             + u_hat * (self.fmask[:,:,prox_idx] == 0)
                else:
                    p_Mhat = p_Mhat * self.fmask[prox_idx] \
                             + u_hat * (self.fmask[prox_idx] == 0)
            u_new = self.inverse_propagator.eval(p_Mhat, prox_idx)
        else:
            u_new = u
        return u_new
