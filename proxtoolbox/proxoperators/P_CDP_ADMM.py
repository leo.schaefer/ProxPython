
from proxtoolbox.proxoperators.ADMM_prox import ADMM_Context
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.size import size_matlab
from proxtoolbox.utils import fft, ifft
from proxtoolbox.utils.accessors import accessors
import numpy as np
from numpy import sqrt, conj, tile, mean, exp, angle, trace, reshape
from numpy.linalg import norm
from numpy.fft import fft2, ifft2

class P_CDP_ADMM(ProxOperator, ADMM_Context):
    """
    Prox operator that projects onto Fourier magnitude constraints
    with nonunitary, but invertible masks. 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Sept. 27, 2016.    
    """

    def __init__(self, experiment):
        # because of multiple inheritance we call explicitely
        # the __init__ method of the two parent classes 
        ProxOperator.__init__(self, experiment)
        ADMM_Context.__init__(self, experiment)
        self.data = experiment.data

    def eval(self, u, prox_index = None):
        """
        Projection onto Fourier magnitude constraints
        with nonunitary, but invertible masks        

        Parameters
        ----------
        u : Cell or ndarray
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : Cell or ndarray
            The projection
        """
        if isCell(u[2]):
            u_image = Cell(len(u[2]))
        else:
            u_image = np.empty(u[2].shape, dtype=u[2].dtype)
        eta = self.lmbda

        if self.Nx == 1 or self.Ny == 1:
            FFT = lambda u: fft(u)
            IFFT = lambda u: ifft(u)
        else:
            FFT = lambda u: fft2(u)
            IFFT = lambda u: ifft2(u)
        
        L = self.product_space_dimension
        get, set, _data_shape = accessors(u[2], self.Nx, self.Ny, L)
        for j in range(L):
            masks_j = get(self.masks,j)
            data_j = get(self.data, j)
            U = FFT(masks_j*u[0])
            U0 = magproj(data_j, eta*U+get(u[2],j))
            u_image_j = IFFT(U0)/masks_j
            set(u_image, j, u_image_j)

        return u_image

 