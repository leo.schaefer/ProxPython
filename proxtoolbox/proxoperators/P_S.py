
from proxtoolbox.proxoperators.proxoperator import ProxOperator
import numpy as np
from numpy import zeros, real, shape

class P_S(ProxOperator):
    """
    Projection onto support constraints
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on May 23, 2002.
    """

    def __init__(self, experiment):
        self.support_idx = experiment.support_idx

    def eval(self, u, prox_idx=None):
        """
        Projects the input data onto nonnegativity and
        support constraints.

        Parameters
        ----------
        u : ndarray
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        p_S : ndarray
            The projection
        """
        p_S = zeros(u.shape, dtype=u.dtype)
        p_S[self.support_idx] = u[self.support_idx]
        return p_S


