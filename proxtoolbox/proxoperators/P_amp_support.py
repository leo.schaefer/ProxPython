
from proxtoolbox.proxoperators.proxoperator import ProxOperator, magproj
from proxtoolbox.utils.cell import Cell, isCell

class P_amp_support(ProxOperator):
    """
    Projection onto support constraints
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on July 26, 2011.
    """

    def __init__(self, experiment):
        self.supp_phase = None
        if hasattr(experiment, 'supp_phase'):
            self.supp_phase = experiment.supp_phase
        self.abs_illumination = experiment.abs_illumination

    def eval(self, u, prox_idx = None):
        """
        Projects the input data onto support constraints.
        'abs_illumination' is the object domain constraint
        (0-1 indicator function)

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        p_amp : ndarray or a list of ndarray objects
            The projection
        """
        if self.supp_phase is not None:
            p_amp = self.abs_illumination #TODO does not make a copy
            if isCell(self.supp_phase):
                index = self.supp_phase[0]
                p_amp[index] = magproj(self.abs_illumination[index], u[index])
            else:
                index = self.supp_phase
                p_amp[index] = magproj(self.abs_illumination[index], u[index])
        else:
            p_amp = magproj(self.abs_illumination, u)
        return p_amp

