
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab
import numpy as np
from numpy import zeros, ones, empty, sum

class P_diag(ProxOperator):
    """
    Projection onto the diagonal of a product space
    """

    def __init__(self, experiment):
        self.n_product_Prox = experiment.n_product_Prox # size of the product space

    def eval(self, u, prox_idx = None):
        """
        Projects the input data onto the diagonal of the product space
        given by its dimensions.
        
        Based on Matlab code written by Russell Luke (Inst. Fuer 
        Numerische und Angewandte Mathematik, Universitaet
        Gottingen) on July 26, 2011.

        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        u_diag : ndarray or a list of ndarray objects
            The projection
        """
        K = self.n_product_Prox
        if not isCell(u):
            m, n, p, q = size_matlab(u)
            # used old python code based on Matlab's master branch:
            # TODO test
            if n == K:
                tmp = sum(u, axis=1, dtype = u.dtype)/K
                # u_diag = tmp * ones((1, K), dtype = u.dtype)
                u_diag = np.matmul(tmp.reshape(tmp.size, 1), ones((1, K), dtype = u.dtype))
            elif p == K:
                tmp = zeros((m,n), dtype = u.dtype)
                for k in range(K):
                    tmp += u[:, :, k]
                tmp /= K
                u_diag = empty((m,n,K), dtype = u.dtype)
                for k in range(K):
                    u_diag[:, :, k] = tmp
            else:
                tmp = zeros((m,n,p), dtype = u.dtype)
                for k in range(K):
                    tmp += u[:, :, :, k]
                tmp /= K
                u_diag = empty((m,n,p,K), dtype = u.dtype)
                for k in range(K):
                    u_diag[:, :, :, k] = tmp
        else:
            # we need to determine the data type needed to
            # compute the sum since the different arrays
            # contained in list 'u' may have different data types.
            # Here we assume that we are dealing either with real or
            # complex data
            dtype = np.dtype('float64') # default data type
            for j in range(len(u)):
                if np.iscomplexobj(u[j]):
                    dtype = u[j].dtype
                    break   
            tmp = zeros(u[0].shape, dtype = dtype)
            N = len(u)
            for j in range(N):
                tmp += u[j]
            tmp /= N
            u_diag = Cell(N)
            for j in range(N):
                u_diag[j] = tmp
        return u_diag

