
from proxtoolbox.proxoperators.proxoperator import ProxOperator
import numpy as np
from numpy import zeros, real, shape

class Pphase_phasepack(ProxOperator):
    """
    Projection onto phase magnitude constraints as done in
    the phasepack toolbox. See the main 
    [PhasePack page](http://cs.umd.edu/~tomg/projects/phasepack/) 
    for complete information, or check out the [user guide]
    (https://arxiv.org/abs/1711.09777).
    
    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Jan 20, 2019.
    """

    def __init__(self, experiment):
        self.phasepack_A = experiment.phasepack_A
        self.phasepack_Apinv = experiment.phasepack_Apinv
        self.data = experiment.data

    def eval(self, u, prox_idx=None):
        """
        Projects the input data onto onto phase magnitude constraints

        Parameters
        ----------
        u : ndarray
            Function in the physical domain to be projected
        prox_idx : int, optional
            Index of this prox operator
        
        Returns
        -------
        u_new : ndarray
            The projection in the physical (time) domain in the
            same format as `u`
        """
        u_new = self.phasepack_A @ u
        u_new = self.phasepack_Apinv @ (self.data * u_new / abs(u_new))
        return u_new

