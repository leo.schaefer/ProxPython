import numpy as np
from numpy import zeros
from proxtoolbox.proxoperators.proxoperator import ProxOperator
from proxtoolbox import proxoperators
from proxtoolbox.utils.cell import Cell, isCell
from proxtoolbox.utils.size import size_matlab

class Prox_product_space(ProxOperator):
    """
    Prox operator in a product space: 
       x=(x_1,x_2, dots, x_n) where x_j in R^{n_j}
       f = (f_1,f_2,...,f_n) where f_j: R^{n_j}--> (-infty,+infty]
       and 
       Prox_f(x) = (Prox_{f_1}(x_1), Prox_{f_2}(x_2),...,Prox_{f_n}(x_n)) 

    Based on Matlab code written by Russell Luke (Inst. Fuer 
    Numerische und Angewandte Mathematik, Universitaet
    Gottingen) on Oct 26, 2017.    
    """

    def __init__(self, experiment):
        # instantiate product prox operators
        self.proxOps = []
        for prox_item in experiment.productProxOperators:
            if isinstance(prox_item, str):
                proxClass = getattr(proxoperators, prox_item)
            else:
                proxClass = prox_item
            prox = proxClass(experiment)
            self.proxOps.append(prox)

    def eval(self, u, prox_index = None):
        """
        Projection subroutine onto constraints arranged in a product space.        
            
        Parameters
        ----------
        u : ndarray or a list of ndarray objects
            Input data to be projected
        prox_idx : int, optional
            Index of this prox operator
         
        Returns
        -------
        u_new : ndarray or a list of ndarray objects
            The prox mapping in the product space.
        """

        K = len(self.proxOps) # the size of the product space
        if isCell(u):
            u_new = Cell(len(u))
            k = 0
            for prox_k in self.proxOps:
                u_new[k] = prox_k.eval(u[k], k)
                k += 1
        else:
            m, n, p, q = size_matlab(u)
            if n == K:
                u_new = zeros((m,K), dtype = u.dtype)
                for k in range(K):
                    u_new[:,k] = self.proxOps[k].eval(u[:,k], k)
            elif p == K:
                u_new = zeros((m,n,K), dtype = u.dtype)        
                for k in range(K):
                    u_new[:,:,k] = self.proxOps[k].eval(u[:,:,k], k)
            elif q == K:
                u_new = zeros((m,n,p,K), dtype = u.dtype) 
                for k in range(K):
                    u_new[:,:,:,k] = self.proxOps[k].eval(u[:,:,:,k], k)
        return u_new



