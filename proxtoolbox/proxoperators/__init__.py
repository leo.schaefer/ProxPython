# -*- coding: utf-8 -*-
"""
This package contains the ProxOperator abstract class and various concrete classes.
"""

from .proxoperator import *
from .Prox_product_space import *
from .Approx_Pphase_FreFra_Poisson import *
from .P_CDP import *
from .P_avg import *
from .P_diag import *
from .P_amp_support import *
from .P_SP import *
from .P_Amod import *
from .P_Sparsity import *
from .P_M import *
from .propagators import *
from .ptychographyProx import *
from .sudokuProx import *
from .CT_prox import *
from .ADMM_prox import *
from .Linearized_phaseret_object import *
from .Regularized_phaseret_object import *
from .Approx_Pphase_JWST_Wirt import *
from .P_CDP_ADMM import *
from .P_S import *
from .P_CDP_cyclic import *
from .sourceLocProx import *
from .Pphase_phasepack import *

def __addCls__(alg):
    """
    Add a class to the namespace of this module.
 
    Parameters
    ----------
    cls : class
        The class to be added to this module.
    """
    name = alg.__name__
    assert name not in globals()
    globals()[name] = alg

def addExternalProxOperator(proxClass):
    """
    Add the proxoperator to the known list. It will become available by using the class name as proxoperator_name.
    This is not required for proxoperators that are already imported to this module.
 
    Parameters
    ----------
    proxClass : a class extending the ProxOperator class
        ProxOperator to be added to the list.
    """
    __addCls__(proxClass)

