"""              Flip.py
            written on 10. May 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

             DESCRIPTION:  Flips a 2x2 image
             INPUT: A, an image to be flipped
             OUTPUT: B, the flipped image
             USAGE: B=Flip(A)

            NONSTANDARD FUNCTION CALLS:  Reorder.py


        !!!!!!!!!!! Not sure if it does the job correctly !!!!!!!!!!
"""

from proxtoolbox.utils.Reorder import Reorder
from numpy import arange


def Flip(A):
    return Reorder(Reorder(A))


if __name__ == "__main__":
    A = arange(1, 19).reshape(2, 3, 3)
    print(A)
    print(Flip(A))
