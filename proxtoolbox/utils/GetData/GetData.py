from proxtoolbox.folders import InputFolder
import sys
import urllib.request
import tarfile

#shows progress of download
def dlProgress(counter, blocksize, size):
    p = counter*blocksize*100.0/size
    sys.stdout.write("\rProgress: %d%%" % p)
    sys.stdout.flush()

#function to ask permission for donwload from user
def query_yes_no(question):
   choices = "[y/n] "
   valid_answer = {'y':True, 'ye':True, 'yes':True, 'n':False, 'ne':False}

   while True:
    sys.stdout.write(question + choices)
    answer = input().lower()
    if answer in valid_answer:
        return valid_answer[answer]
    else:
        sys.stdout.write("Please respond with 'yes' or 'no'. \n")

#downloads the input data to the InputData folder. problemFamily has to be either Phase, CT or Ptychography
def getData(problemFamily):
	if problemFamily == 'Phase':
		my_file = InputFolder / "Phase/pupil.pmod"
	elif problemFamily == 'CT':
	    	my_file = InputFolder / "CT/ART_SheppLogan.mat"
	elif problemFamily == 'Ptychography':
		my_file = InputFolder / "Ptychography/gaenseliesel.png"
	else:
		print("Invalid input in GetData.GetData. problemFamily has to be Phase, CT or Ptychography")
		return -1

	if not(my_file.is_file()):
		print(problemFamily + " input data is missing.") 
		if query_yes_no("Do you want to download the " + problemFamily + " input data?"):
		    urllib.request.urlretrieve(" http://vaopt.math.uni-goettingen.de/data/" + problemFamily +  ".tar.gz", InputFolder / (problemFamily +  ".tar.gz"), reporthook=dlProgress)
		    print("\nExtracting data...")
		    tar = tarfile.open(InputFolder / (problemFamily +  ".tar.gz"), "r:gz")
		    tar.extractall(InputFolder / problemFamily)
		    tar.close()
		if not(my_file.is_file()):
			print('***************************************************************************************')
			print('* Input data still missing.  Please try automatic download again or manually download *') 
			print('*     http://vaopt.math.uni-goettingen.de/data/' + problemFamily +  '.tar.gz                           *')
			print('* Save and unpack the ' + problemFamily +  '.tar.gz datafile in the                                    *')
			print('*    ProxMatlab/InputData subdirectory                                                *')
			print('***************************************************************************************')
