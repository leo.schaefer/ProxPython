from numpy import array, shape, meshgrid, roll
from numpy.fft import fft2, ifft2
from scipy import floor, pi, exp, sqrt

def IFresnel2(A):
    """              IFresnel2
            written on 3. May  2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: < Description >
    Input: A, n x m numpy array 
            

    Output: E_out, nxm numpy array
    Usage: E_out = IFresnel2(A) """



    Nx, Ny = shape(A)

    z_eff = 0.0088         # distance object plane to observation plane [m]
    Lambda = 7.0846e-11    # x-ray wavelength [m]
    d1x = 1.5672e-07       # pixel size in object plane [m]
    d1y = 1.5672e-07


    x02 = floor(Nx/2)+1
    y02 = floor(Ny/2)+1
    x01 = x02
    y01 = y02

    # create Coordinate system
    # Get Frequency coordinates
    
    xd = array(list(map(lambda x: (x-x01)/Nx/d1x, range(1,Nx+1))))
    yd = array(list(map(lambda y: (y-y01)/Ny/d1y, range(1,Ny+1))))
    [Fx,Fy] = meshgrid(xd,-yd)
    del xd, yd

    # calc necessary propagators
    kappa = 2 * pi/Lambda * sqrt(1-Lambda**2 * (Fx**2 + Fy**2))
    
    #forward
    H = exp(sqrt(-1) * kappa * abs(z_eff))
    
    
    #proagate from sample plane to detector plane
    E_out = fft2(A)
    E_out = roll(E_out, int(y01-1), axis=1)
    E_out = roll(E_out, int(x01-1), axis=0) * H.T
    
    E_out = roll(E_out, int(-(y01-1)), axis=1)
    E_out = roll(E_out, int(-(x01-1)), axis=0)
    


    return ifft2(E_out)
    
    
if __name__ == "__main__":
    A = array([[1,2,3,4,5], [4,-2,3,10,0]]).reshape(2,5)
    print(IFresnel2(A))




