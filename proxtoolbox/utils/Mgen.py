"""              Mgen.py
            written on 10. May 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen
            
             Usage: A = Mgen(k,n,s,c)
             Inputs: rows, k; columns, n; scaling, s; center, c; seed, seed.
             Output: Random matrix with elements uniformly distributed on [c-s, c+s].


    
    
 """
#from random import seed as ran_seed
from numpy.random import seed as ran_seed, rand
from numpy import ones
 
def Mgen(k, n, s, c, seed):
    # rand('state',sum(100*clock))
    ran_seed(seed)              # set the seed for the random generator to "seed"
                                    # not quite like rand('state', seed); in the .m file
                                    
    return s * (rand(k, n) - 0.5 * ones((k,n)))+c
    
    
    
if __name__ == "__main__":
    print(Mgen(2,2,1,0,3036))
