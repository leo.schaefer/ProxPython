"""              dftregistration
            written on Sep 4. 2019 by
            Constantin Höing
            Inst. Fuer Numerische und Angewandte Mathematik
            Universität Göttingen

    Description: COPIED FROM MATLAB
    
            % function [output Greg] = dftregistration(buf1ft,buf2ft,usfac);
            % Efficient subpixel image registration by crosscorrelation. This code
            % gives the same precision as the FFT upsampled cross correlation in a
            % small fraction of the computation time and with reduced memory 
            % requirements. It obtains an initial estimate of the crosscorrelation peak
            % by an FFT and then refines the shift estimation by upsampling the DFT
            % only in a small neighborhood of that estimate by means of a 
            % matrix-multiply DFT. With this procedure all the image points are used to
            % compute the upsampled crosscorrelation.
            % Manuel Guizar - Dec 13, 2007
            
            % Portions of this code were taken from code written by Ann M. Kowalczyk 
            % and James R. Fienup. 
            % J.R. Fienup and A.M. Kowalczyk, "Phase retrieval for a complex-valued 
            % object by using a low-resolution image," J. Opt. Soc. Am. A 7, 450-458 
            % (1990).
            
            % Citation for this algorithm:
            % Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup, 
            % "Efficient subpixel image registration algorithms," Opt. Lett. 33, 
            % 156-158 (2008).



    Input: buf1ft    Fourier transform of reference image, 
                   DC in (1,1)   [DO NOT FFTSHIFT]
         buf2ft    Fourier transform of image to register, 
                   DC in (1,1) [DO NOT FFTSHIFT]
         usfac     Upsampling factor (integer). Images will be registered to 
                   within 1/usfac of a pixel. For example usfac = 20 means the
                   images will be registered within 1/20 of a pixel. (default = 1)


    Output: output =  [error,diffphase,net_row_shift,net_col_shift]
             error     Translation invariant normalized RMS error between f and g
             diffphase     Global phase difference between the two images (should be
                           zero if images are non-negative).
             net_row_shift net_col_shift   Pixel shifts between images
             Greg      (Optional) Fourier transform of registered version of buf2ft,
                       the global phase difference is compensated for.

    Usage: [output, Greg] = dftregistration(buf1ft,buf2ft,usfac=1) """
    

import numpy as np
    
def dftregistration(buf1ft,buf2ft,usfac=1):
    
    Greg = None # set to None if not initialized
    if usfac == 0:
        CCmax = sum(sum(buf1ft * np.conj(buf2ft)))
        rfzero = sum(abs(np.square(buf1ft.flatten())))
        rgzero = sum(abs(np.square(buf2ft.flatten())))  # correct ?
        
        error = 1.0 - CCmax*np.conj(CCmax)/(rgzero*rfzero); 
        error = np.sqrt(abs(error));
        
        diffphase = np.arctan2(np.imag(CCmax * np.conj(CCmax)), np.real(CCmax))
        output = [error, diffphase]
        
    elif usfac == 1:
        [m,n] = np.shape(buf1ft)
        CC = np.fft.ifft2(buf1ft * np.conj(buf2ft))
        #print(CC)
        [max1, loc1] = [np.amax(CC, axis=0), [np.argmax(CC[i]) for i in range(np.shape(CC)[0]) ] ]
        
        [max2, loc2] = [np.max(max1), np.argmax(max1)]
        
        rloc = loc1[loc2]
        cloc = loc2
        CCmax = CC[rloc, cloc]
        #print(buf1ft.flatten())
        rfzero = sum(np.square(np.abs(buf1ft.flatten()))) / (m*n)
        rgzero = sum(np.square(np.abs(buf2ft.flatten()))) / (m*n)
        
        
        error = 1 - CCmax * np.conj(CCmax) / (rgzero * rfzero)
        error = np.sqrt(np.abs(error))
        
        diffphase = np.arctan2(np.imag(CCmax), np.real(CCmax))
        print(error)
        
        
        
        md2 = np.fix(m/2)
        nd2 = np.fix(n/2)
        
        if rloc > md2:
            row_shift = rloc - m 
        else:
            row_shift = rloc 
        
        if cloc > nd2:
            col_shift = cloc - n 
        else:
            col_shift = cloc 
            
        output = [error, diffphase, row_shift, col_shift]
        #print(output)
        
    else:
        [m,n] = np.shape(buf1ft)
        mlarge = m *2
        nlarge = n*2
        CC = np.zeros((mlarge, nlarge))
        #print(n+np.fix((n-1)/2))
        CC[int(m-np.fix(m/2)):int(m+np.fix((m)/2)),int(n-np.fix(n/2)):int(n+np.fix((n)/2))] = np.fft.fftshift(buf1ft) * np.conj(np.fft.fftshift(buf2ft))  # index ???
            
        
        # Compute crosscorrelation and locate the peak 
        
        CC = np.fft.ifft2(np.fft.ifftshift(CC))  # Calculate cross-correlation
        [max1, loc1] = [np.amax(CC, axis=0), [np.argmax(CC[i]) for i in range(np.shape(CC)[0]) ] ]
        
        [max2, loc2] = [np.max(max1), np.argmax(max1)]
        rloc=loc1[loc2]
        cloc=loc2
        CCmax=CC[rloc,cloc]
        
        #Obtain shift in original pixel grid from the position of the
        #crosscorrelation peak

        [m,n] = np.shape(CC)
        md2 = np.fix(m/2)
        nd2 = np.fix(n/2)
        
        if rloc > md2:
            row_shift = rloc -m 
        else:
            row_shift = rloc 
            
        if cloc > nd2:
            col_shift= cloc - n 
        else:
            col_shift = cloc 

        row_shift=row_shift/2
        col_shift=col_shift/2
        
        
        #  If upsampling > 2, then refine estimate with matrix multiply DFT
        if usfac > 2:
            
            ### DFT computation ###
            # Initial shift estimate in upsampled grid
            row_shift = np.round(row_shift * usfac) / usfac
            col_shift = np.round(col_shift * usfac) /usfac
            
            dftshift = np.fix(np.ceil(usfac * 1.5)/2) #  Center of output array at dftshift+1
            # Matrix multiply DFT around the current shift estimate
        
        
        
            CC = np.conj(dftups(buf2ft*np.conj(buf1ft),np.ceil(usfac*1.5),np.ceil(usfac*1.5),usfac, dftshift-row_shift*usfac,dftshift-col_shift*usfac))/(md2*nd2*usfac**2)
        
            
            [max1, loc1] = [np.amax(CC, axis=0), [np.argmax(CC[i]) for i in range(np.shape(CC)[0]) ] ]
        
            [max2, loc2] = [np.max(max1), np.argmax(max1)]
            rloc=loc1[loc2]
            cloc=loc2
            #print(CC)
            CCmax=CC[rloc,cloc]
            
            rg00 = dftups(buf1ft*np.conj(buf1ft),1,1,usfac)/(md2*nd2*usfac**2)
            rf00 = dftups(buf2ft*np.conj(buf2ft),1,1,usfac)/(md2*nd2*usfac**2)  
            rloc = rloc - dftshift # !!!!!!!!!!!!!!!
            cloc = cloc - dftshift #!!!!!!!!!!!!!!!!!!!
            row_shift = row_shift + rloc/usfac
            col_shift = col_shift + cloc/usfac    
            
            #If upsampling = 2, no additional pixel shift refinement
            
        else:    
            rg00 = sum(sum( buf1ft*np.conj(buf1ft) ))/m/n
            rf00 = sum(sum( buf2ft*np.conj(buf2ft) ))/m/n
                
        error = 1.0 - CCmax*np.conj(CCmax)/(rg00*rf00)
        error = np.sqrt(abs(error))[0][0]
        diffphase=np.arctan2(np.imag(CCmax),np.real(CCmax))
            # If its only one row or column the shift along that dimension has no
            # effect. We set to zero.
            
        if md2 == 1:
            row_shift = 0
        
        if nd2 == 1:
            col_shift = 0
        
        output=[error,diffphase,row_shift,col_shift]
        
        #Compute registered version of buf2ft
    
    if nargout() > 1 and usfac > 0:
        [nr, nc] = np.shape(buf2ft)
        Nr = np.fft.ifftshift(np.arange(-np.fix(nr/2) , np.ceil(nr/2))) #!!
        Nc = np.fft.ifftshift(np.arange(-np.fix(nc/2),np.ceil(nc/2))) #!!
        [Nc,Nr] = np.meshgrid(Nc,Nr)
        
        Greg = buf2ft*np.exp(1j*2*np.pi*(-row_shift*Nr/nr-col_shift*Nc/nc))
        Greg = Greg*np.exp(1j*diffphase)
        return [output, Greg]
        
    elif nargout() > 1 and usfac == 0:
        Greg = buf2ft  * np.exp(1j*diffphase)
        return [output, Greg]
    
    return output
        
    
    



def dftups(In, nor=None, noc=None, usfac=1, roff=0, coff=0):
    
    """
            COPIED FROM MATLAB
            
            % function out=dftups(in,nor,noc,usfac,roff,coff);
% Upsampled DFT by matrix multiplies, can compute an upsampled DFT in just
% a small region.
% usfac         Upsampling factor (default usfac = 1)
% [nor,noc]     Number of pixels in the output upsampled DFT, in
%               units of upsampled pixels (default = size(in))
% roff, coff    Row and column offsets, allow to shift the output array to
%               a region of interest on the DFT (default = 0)
% Recieves DC in upper left corner, image center must be in (1,1) 
% Manuel Guizar - Dec 13, 2007
% Modified from dftus, by J.R. Fienup 7/31/06

% This code is intended to provide the same result as if the following
% operations were performed
%   - Embed the array "in" in an array that is usfac times larger in each
%     dimension. ifftshift to bring the center of the image to (1,1).
%   - Take the FFT of the larger array
%   - Extract an [nor, noc] region of the result. Starting with the 
%     [roff+1 coff+1] element.

% It achieves this result by computing the DFT in the output array without
% the need to zeropad. Much faster and memory efficient than the
% zero-padded FFT approach if [nor noc] are much smaller than [nr*usfac nc*usfac]

    
    
    """
    
    [nr, nc] = np.shape(In)
    
    #set defaults
    #if noc != 1:
    #    noc = nc
    #if nor != 1:
    #    nor = nr
        
    # Compute kernels and obtain DFT by matrix products
    #print(nr)
    A = (np.fft.ifftshift(np.arange(0,nc)).T - np.floor(nc/2)).reshape(nc, 1)
    B = (np.arange(0,noc) - coff).reshape(1, int(noc))
    #print(A@ B)
    kernc=np.exp((-1j*2*np.pi/(nc*usfac))* A @ B ) #!!!!!!!!!!!!!
    A = ( np.arange(0,nor).T - roff ).reshape(int(nor), 1)
    B = ( np.fft.ifftshift(np.arange(0,nr)) - np.floor(nr/2)  ).reshape(1, nr)
    kernr=np.exp((-1j*2*np.pi/(nr*usfac))* A @ B )
    
    #print(kernr@In@kernc)
    #print("\n\n\n")
    return kernr@In@kernc



def nargout():
   import traceback
   callInfo = traceback.extract_stack()
   callLine = str(callInfo[-3].line)
   split_equal = callLine.split('=')
   split_comma = split_equal[0].split(',')
   return len(split_comma)
    

if __name__ == "__main__":
    #A = np.arange(16).reshape(4,4)
    #B = np.arange(4, 20).reshape(4,4)
    
    A = np.array([[1,0,5,9], [2,0,6,5], [3,0,7,2], [5,6,7,8]])
    B = A *2
    
    #print(B)
    (a, b) = dftregistration(A, B, 4)
    print(a, b)
            
            