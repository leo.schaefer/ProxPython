import h5py
from numpy import array, ndarray, int_, float_, complex_, string_

__all__ = ['write_dict_to_h5', 'load_h5', 'h5_group_to_dict', 'prep_item_for_h5', 'decode_h5_items']


# TODO: deal with attributes in HDF5 format?
def write_dict_to_h5(h5file, dictionary: dict, path: str = '/', skip_keys: any = None, close_file: bool = True,
                     **kwargs):
    """
    Given a python dictionary, write all items to a h5 file.
    Most items are stored as datasets (str, float, int are cast to one-length numpy arrays,
    None-type written as str('None')), while dictionary-type items are written recursively as lower-level groups.

    :param h5file: file handle or path to use in creation
    :param dictionary: data to be saved
    :param path: path in h5 file, where to store data
    :param skip_keys: list of keys which to ignore in the dictionary
    :param close_file: close file after writing?
    :param kwargs: passed on to file creation
    :return: file handle if file remains open
    """
    if skip_keys is None:
        skip_keys = ['']
    if type(h5file) == str:
        # Create file to write to
        _mode = kwargs.pop('mode', 'a')
        _h5file = h5py.File(h5file, mode=_mode, **kwargs)
    else:
        _h5file = h5file

    if not path[-1] == '/':
        path += '/'

    cur_group = _h5file[path]
    for key, val in dictionary.items():
        # Skip some keys:
        if key in skip_keys:
            continue
        # Create subgroup for dictionary-type items:
        if type(val) == dict:
            _h5file.create_group(path + key)
            # Fill the subgroup, keep file open!
            write_dict_to_h5(_h5file, val, path=path + key,
                             skip_keys=skip_keys, close_file=False, compression='lzf')
            continue
        # Some rules for object serialization:
        save_obj = prep_item_for_h5(val)
        # Write the dataset element
        try:
            cur_group.create_dataset(name=path + key, data=save_obj)
        except TypeError as e:
            raise TypeError(('Failed to write object %s: ' % (path + key)) + repr(e))
        # end loop
    if close_file:
        # This should only happen in the outer-most (first) function call in recursive behaviour.
        _h5file.close()
    else:
        return _h5file


def prep_item_for_h5(item):
    """
    h5py can only handle numpy arrays of certain dtypes - cast object to understandable type here
    """
    if item is None:
        # Serialize None as string
        item = 'None'
    if type(item) == str:
        # Empty strings are not possible
        if item == '':
            item = ' '
        save_obj = array([item], dtype='S')
    elif type(item) in (int, float, int_, float_, complex_, bool):
        # create array object
        save_obj = array([item])
    elif isinstance(item, (list, ndarray)) and len(item) > 0 and type(item[0]) == str:
        # for strings, use 'S'-type
        save_obj = array(item, dtype='S')
    else:
        save_obj = item
    return save_obj


def load_h5(filepath, keys=None, skip_keys=None, decode=True, **kwargs):
    """
    Load items into a dict from an h5 file. Kwargs are passed to h5_group_to_dict

    :param filepath: path to file
    :param keys: list of keys to load (ignore all others)
    :param skip_keys: list of keys to skip
    :param decode: Attempt to parse items into logical types through decode_h5_items
    :param kwargs: are passed on to h5_group_to_dict along with keys, skip_keys and decode
    :return: dictionary of items
    """
    kwargs['keys'] = keys
    kwargs["skip_keys"] = skip_keys
    kwargs['decode'] = decode
    fl = h5py.File(filepath, mode='r')
    out = h5_group_to_dict(fl, **kwargs)
    fl.close()
    return out


def h5_group_to_dict(group, keys=None, skip_keys=None, decode=True):
    """
    Cast group to a dictionary, subgroups become sub-dictionaries
    Note that zero-length items will be skipped

    :param group: h5 group (can be a file handle)
    :param keys: list of keys to load (ignore all others)
    :param skip_keys: list of keys to skip
    :param decode: Attempt to parse items into logical types through decode_h5_items
    :return: dictionary of items
    """
    out = {}
    if skip_keys is None:
        skip_keys = ['']
    for key, val in group.items():
        # skip key if given in skip_keys (also skips subgroups)
        if key in skip_keys:
            continue
        # recursively apply to subgroups:
        if type(val) == h5py.Group:
            out[key] = h5_group_to_dict(val, keys=keys, skip_keys=skip_keys, decode=decode)
            continue
        # take only selected keys if key-kwarg is given (does not apply to subgroups)
        if keys is not None and key not in keys:
            continue
        # take all items and store in key-value pair:
        if decode:
            out[key] = decode_h5_items(val)
        elif not val.shape == ():
            out[key] = val[:]
        else:
            out[key] = None
    return out


def decode_h5_items(item):
    """Reverse prep_item_for_h5 after loading a h5 file"""
    if item.shape == ():
        # Zero-length object are empty; parse 'None' strings to None
        out = None
    elif item.shape == (0,):
        out = None
    elif item.shape == (1,):
        # Single-item ndarrays should be unpacked to just the value
        out = item[0]
    elif isinstance(item[0], string_):
        # cast string-type data to default strings
        out = list(item[:].astype(str))
    else:
        out = item[:]
    if (isinstance(out, str) and out == 'None') or (isinstance(out, bytes) and out == b'None'):
        out = None
    return out
