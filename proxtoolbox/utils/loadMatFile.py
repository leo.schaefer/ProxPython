
import numpy as np
import h5py
from scipy.io import loadmat
from proxtoolbox.utils.cell import Cell, isCell
from pathlib import Path

def loadMatFile(filename):
    '''
    This utility function loads .mat Matlab files, returning
    a dictionary representing the content of the .mat file.
    .mat file version 7.3 are supported. For older versions,
    this implementation uses scipy.io.loadmat().

    Parameters
    ----------
    filename : str or Path
        Name of the .mat file

    Returns
    -------
    mat_dict  dict
        Dictionary with variable names as keys, and loaded matrices as values.
 
    Notes
    -----
    When reading v7.3 files cell objects are not currently supported.
    '''

    path = Path(filename)
    if not path.name.endswith('.mat'):
        path = path.parent / (path.name+'.mat')

    if not path.is_file():
        errMsg = "File " + path + " was not found"
        raise IOError(errMsg)

    try:
        # try scipy loadMat first
        return loadmat(path)
    except NotImplementedError:
        try:
            # try to read it as a v7.3 file:
            with h5py.File(path, 'r') as file:
                return MatFile73Reader(file).read()
        except OSError:
            raise TypeError('Did not recognize this version of .mat file')


class MatFile73Reader:
    ''' 
    Reader for Mat 7.3 mat files. To read .mat files, use
    loadMatFile() above.
    '''
    
    def __init__(self, file):
        self.file = file

    def read(self):
        mat_dict = {}
        self.readItems(self.file, mat_dict)
        return mat_dict

    def readItems(self, parentItem, mat_dict):
        for varName in parentItem:
            if varName[0] == '#': # filter out any item starting with #
                continue
            varItem = parentItem[varName]
            if type(varItem) is h5py.Dataset:
                # decode dataset item
                mat_dict[varName] = self.decodeDataset(varItem)
            else:
                # create sub dictionary
                mat_dict[varName] = {}
                self.readItems(varItem, mat_dict[varName]) # use recursion

    def decodeDataset(self, dataset):
        # determine dataset type
        matlab_type = None
        if isinstance(dataset[0][0], h5py.h5r.Reference):
            # this is a cell
            matlab_type = 'cell' # TODO not supported
        elif 'imag' in str(dataset.dtype): 
            # since there is no complex type (only tupple of doubles)
            matlab_type = 'complex'
        elif 'MATLAB_class' in dataset.attrs:
            matlab_type = dataset.attrs['MATLAB_class'].decode()
        
        # decode 
        if matlab_type in ('double', 'single', 'int8', 'int16', 'int32', 'int64', 
                        'uint8', 'uint16', 'uint32', 'uint64'):
            # convert it into an numpy array and then squeeze 
            arr = np.array(dataset[()], dtype=dataset.dtype)
            if arr.size == 1:
                value = arr.item()
            else:
                value = arr.T.squeeze()
        elif matlab_type == 'complex':
                arr = np.array(dataset)
                arr = (arr['real'] + arr['imag']*1j).astype(np.complex128)
                if arr.size == 1:
                    value = arr.item()
                else:
                    value = arr.T.squeeze()
        elif matlab_type == 'char':
            st = ''
            for c_item in dataset:
                # c_item is an array
                for c in c_item:
                    st += chr(c)
            st.replace('\x00', '')
            value = st
        elif matlab_type == 'logical':
            arr = np.array(dataset[()], dtype=bool)
            if arr.size == 1:
                value = arr.item()
            else:
                value = arr.T.squeeze()
        else:
            print('Warning: matlab data type not supported ', matlab_type, ' ',
                dataset.dtype, ' in ', dataset.name)
            value = None
        return value
    