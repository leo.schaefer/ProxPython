
# Detect what type of kernel is running
__is_IPython__ = False
try:
    shell = get_ipython().__class__.__name__
    if shell == 'ZMQInteractiveShell':
        __is_IPython__ = True   # Jupyter notebook or qtconsole
    elif shell == 'TerminalInteractiveShell':
        pass                    # Terminal running IPython ???? testen mit plt
except NameError:
    pass                        # Probably standard Python interpreter

if __is_IPython__:
    import IPython

def isIPython():
    return __is_IPython__

def getDisplay():
    return IPython.display
