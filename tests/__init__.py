"""
Provides the function `runTests()` that will execute on tagged commits.
"""

def runTests():
    print("Running tests...")

__all__ = ["runTests"]